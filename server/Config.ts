const port = parseInt(process.env.PORT || "5000");
const isDev = process.env.NODE_ENV === "development";

export const WorkerSetupWait = 10000;
export const WorkerRestartAttempts = 5;

export const RunnerTimeout = 5000;
export const CalculationTimeout = isDev ? 10000 : 10 * 60 * 1000;

export const LogLevel = process.env.LOG_LEVEL || "verbose";
export const ExposePort = isNaN(port) ? 5000 : port;
export const DatabaseConnectionString = process.env.DATABASE || "mongodb://localhost:27017/signalyser";
