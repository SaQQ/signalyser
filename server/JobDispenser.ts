import * as winston from "winston";
import * as db from "./Db/Mongo";
import { ClusterController } from "./Computations/ClusterController";

export class JobDispenser {

    private readonly controller = new ClusterController();

    reactToProtocolChange(protoId: string) {
        winston.verbose("Protocol %s changed, ignoring the event", protoId);
    }

    async recomputeExam(protoId: string, examId: string) {
        winston.verbose("Examination %s of protocol %s changed, dispatching jobs for all parameters", examId, protoId);

        const proto = await db.Protocols.find(protoId);
        if (proto.type === "DbActionError") {
            winston.error("Cannot read protocol %s, error: %s", protoId, proto.error);
        } else {
            proto.result.params.forEach(param => {
                this.controller.addJob(protoId, examId, param.name);
            });
        }
    }

    start() {
        return this.controller.start();
    }

    stop() {
        return this.controller.stop();
    }

}

export const Dispenser = new JobDispenser();
