import { Request, Response } from "express";

import { Examination, ExaminationWithTags } from "./Contract";
import { Dispenser } from "./JobDispenser";
import { translateCreate, translateUpdate, translateResult, translateError } from "./Utils/Translators";

import * as db from "./Db";
import * as validators from "./Validators";

export async function list(req: Request, res: Response) {
    const protoId = req.params.protocolId as string;
    const filter = req.body as Object;

    if (!validators.validateId(res, protoId) ||
        !validators.validateFilter(res, filter)) {
        return res;
    }

    let result = await db.Examinations.all(protoId, filter);
    return translateResult(res, result);
}

export async function get(req: Request, res: Response) {
    const protoId = req.params.protocolId as string;
    const examId = req.params.examId as string;
    if (!validators.validateId(res, protoId) ||
        !validators.validateId(res, examId)) {
        return res;
    }

    let result = await db.Examinations.find(protoId, examId);
    return translateResult(res, result);
}

export async function add(req: Request, res: Response) {
    let protoId = req.params.protocolId as string;
    let exam = req.body as ExaminationWithTags;

    if (!validators.validateId(res, protoId) ||
        !validators.validateExamination(res, exam)) {
        return res;
    }
    let result = await db.Examinations.add(protoId, exam);
    return translateCreate(res, result);
}

export async function update(req: Request, res: Response) {
    let protoId = req.params.protocolId as string;
    let examId = req.params.examId as string;
    let exam = req.body as ExaminationWithTags;
    if (!validators.validateId(res, protoId) ||
        !validators.validateId(res, examId) ||
        !validators.validateExamination(res, exam)) {
        return res.sendStatus(400);
    }
    let result = await db.Examinations.update(protoId, examId, exam);
    return translateUpdate(res, result);
}

export async function recompute(req: Request, res: Response) {
    let protoId = req.params.protocolId as string;
    let examId = req.params.examId as string;
    if (!validators.validateId(res, protoId) ||
        !validators.validateId(res, examId)) {
        return res.sendStatus(400);
    }
    await Dispenser.recomputeExam(protoId, examId);
    return res.sendStatus(204);
}

export async function recomputeProtocol(req: Request, res: Response) {
    let protoId = req.params.protocolId as string;

    if (!validators.validateId(res, protoId)) {
        return res.sendStatus(400);
    }

    let result = await db.Examinations.all(protoId, {});

    if (result.type === "DbActionError") {
        return translateError(res, result);
    } else if (result.result === undefined) {
        res.sendStatus(204);
    }

    for (let exam of result.result) {
        await Dispenser.recomputeExam(protoId, exam.id);
    }

    return res.status(200).json({ amount: result.result.length });
}
