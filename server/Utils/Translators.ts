import * as is from "./Is";
import { Response } from "express";
import { DbActionError, CreateResult, UpdateResult, ReadResult, ErrorType } from "../Db";

export function translateError(res: Response, r: DbActionError) {
    switch (r.error) {
        case ErrorType.InternalError:
            return res.sendStatus(500);
        case ErrorType.NotFound:
            return res.sendStatus(404);
    }
}

export function checkValidity(res: Response, result: is.ValidationResult) {
    if (is.success(result)) {
        return true;
    } else {
        res.status(422).json(result);
        return false;
    }
}

export function translateUpdate(res: Response, r: UpdateResult) {
    if (r.type === "DbActionError") {
        return translateError(res, r);
    } else {
        return res.status(200).json({ id: r.id });
    }
}

export function translateCreate(res: Response, r: CreateResult) {
    if (r.type === "DbActionError") {
        return translateError(res, r);
    } else {
        return res.status(201).json({ id: r.id });
    }
}

export function translateResult<T>(res: Response, r: ReadResult<T>, skipStatus?: boolean) {
    if (r.type === "DbActionError") {
        return translateError(res, r);
    } else if (r.result === undefined) {
        if (skipStatus !== undefined && skipStatus) {
            return res;
        } else {
            return res.sendStatus(204);
        }
    } else {
        return res.status(200).json(r.result);
    }
}
