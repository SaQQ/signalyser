export interface ValidationError {
    code: number;
    message: string;
}

export interface ValidationSuccess {
    type: "ValidationSuccess";
}

export interface ValidationFailure {
    type: "ValidationFailed";
    errors: { [field: string]: ValidationError[] };
}

export type ValidationResult = ValidationSuccess | ValidationFailure;

export function success(result: ValidationResult): result is ValidationSuccess {
    return result.type === "ValidationSuccess";
}

export function failure(result: ValidationResult): result is ValidationFailure {
    return result.type === "ValidationFailed";
}

export interface CheckOptions {
    fatal?: boolean;
    message?: string;
    code?: number;
}

type Get<T1, T2> = (v: T1) => T2;
type CheckResult = [string, number] | [string, number, boolean] | undefined;
type Checker<T> = (v: T) => CheckResult;
type InnerValidator<T> = (v: BaseCheck<T>) => void;

interface FinalCheck<TBase> {
    andHas<TProp>(get: Get<TBase, TProp>, nameOverride?: string): PropertyCheck<TBase, TProp>;

    result: ValidationResult;
    valid: boolean;
    invalid: boolean;
}

export interface BaseCheck<TBase> {
    readonly data: TBase;

    when(fn: Checker<TBase>, opts?: CheckOptions): AdditionalCheck<TBase>;
    whenHas<TProp>(get: Get<TBase, TProp>, nameOverride?: string): PropertyCheck<TBase, TProp>;

    whenFulfils(fn: InnerValidator<TBase>, opts?: CheckOptions): AdditionalCheck<TBase>;
}

interface AdditionalCheck<TBase> extends FinalCheck<TBase> {
    readonly data: TBase;

    and(fn: Checker<TBase>, opts?: CheckOptions): AdditionalCheck<TBase>;
    andFulfils(fn: InnerValidator<TBase>, opts?: CheckOptions): AdditionalCheck<TBase>;
}

interface PropertyCheck<TBase, TProp> {
    readonly data: TProp;

    that(fn: Checker<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp>;
    thatFulfils(fn: InnerValidator<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp>;
}

interface AdditionalPropertyCheck<TBase, TProp> extends FinalCheck<TBase> {
    readonly data: TProp;

    andThat(fn: Checker<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp>;
    andThatFulfils(fn: InnerValidator<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp>;
}

export function valid<T>(data: T, name?: string) {
    return new BaseValidator(data, new IntermediateResult(name || "", { list: {}, count: 0 }));
}

export function test<T>(fn: (val: T) => boolean, message: string, code: number) {
    return (val: T): CheckResult => {
        if (!fn(val)) {
            return [message, code];
        }
    };
}

interface CheckListBuilder<TCheck, TVal> {
    (v: BaseCheck<TVal>): void;
    and(fn: TCheck): this;
}

export function every<T>(fn: Checker<T>): CheckListBuilder<Checker<T>, T[]> {
    const result: any = function (v: BaseCheck<T[]>) {
        const checkers: Checker<T>[] = result.checkers;
        let len = v.data.length;
        for (let i = 0; i < len; i++) {
            let inner = v.whenHas(d => d[i], `[${i}]`);
            checkers.forEach(c => inner.that(c));
        }
    };
    result.checkers = [fn];
    result.and = function (fn: Checker<T>) {
        result.checkers.push(fn);
        return this;
    };
    return result;
}

export function everyFulfils<T>(fn: InnerValidator<T>): CheckListBuilder<InnerValidator<T>, T[]> {
    const result: any = function (v: BaseCheck<T[]>) {
        const checkers: InnerValidator<T>[] = result.checkers;
        let len = v.data.length;
        for (let i = 0; i < len; i++) {
            let inner = v.whenHas(d => d[i], `[${i}]`);
            checkers.forEach(c => inner.thatFulfils(c));
        }
    };
    result.checkers = [fn];
    result.and = function (fn: InnerValidator<T>) {
        result.checkers.push(fn);
        return this;
    };
    return result;
}

interface Length { length: number; }

export function notNull<T>(val: T): CheckResult {
    if (val === undefined || val === null) {
        return ["The value must be present", 1, true];
    }
}

export function instanceOf<T>(ctor: { new (): T }) {
    return (v: T): CheckResult => {
        if (!(v instanceof ctor)) {
            return ["The value must be an instance of " + ctor.name, 7, true];
        }
    };
}

export const notEmpty = test<Length>(v => v.length > 0, "The value must not be empty", 2);
export const equal = <T>(v: T) => test<T>(v2 => v === v2, "The value must be equal to " + v, 3);
export const longerThan = <T extends Length>(len: number) => test<T>(v => v.length > len, "The value must be longer than " + len, 4);
export const greaterThan = (v: number) => test<number>(v2 => v2 > v, "The value must be greater than " + v, 5);
export const lowerThan = (v: number) => test<number>(v2 => v2 < v, "The value must be lower than " + v, 6);
export const array = instanceOf(Array);
export const match = (r: RegExp) => test<string>(v => r.test(v), "The value must match the expession " + r, 8);

export const fatal: CheckOptions = { fatal: true };

class IntermediateResult {

    constructor(
        private name: string,
        private readonly errors: { list: { [name: string]: ValidationError[] }, count: number }) {
    }

    get errorCount() {
        return this.errors.count;
    }

    get result(): ValidationResult {
        if (this.errors.count === 0) {
            return { type: "ValidationSuccess" };
        } else {
            return { type: "ValidationFailed", errors: this.errors.list };
        }
    }

    derive<T1, T2>(get: Get<T1, T2>, nameOverride?: string) {
        let newName;
        if (nameOverride) {
            newName = this.name.length === 0 ? nameOverride : this.name + nameOverride;
        } else {
            let name = IntermediateResult.extractName(get);
            newName = this.name.length === 0 ? name.substr(1) : this.name + name;
        }
        return new IntermediateResult(newName, this.errors);
    }

    addError(result: [string, number] | [string, number, boolean], opts: CheckOptions) {
        let [message, code] = result;
        message = opts.message || message;
        code = opts.code || code;

        let existing = this.errors.list[this.name];
        if (existing !== undefined) {
            existing.push({ code, message });
        } else {
            this.errors.list[this.name] = [{ code, message }];
        }
        this.errors.count++;

        return this;
    }

    checkAndAdd<T>(fn: Checker<T>, data: T, opts: CheckOptions) {
        const result = fn(data);
        if (result !== undefined) {
            this.addError(result, opts);
            if (result.length === 3 && opts.fatal === undefined) {
                return result[2] as boolean;
            } else {
                return opts.fatal === true;
            }
        }
        return false;
    }

    private static extractName<T1, T2>(fn: Get<T1, T2>) {
        const code = fn.toString();
        let idx = code.indexOf(".");
        if (idx >= 0) {
            return code.substr(idx);
        } else {
            return ".UNKNOWN";
        }
    }
}

class BaseValidator<TBase> implements BaseCheck<TBase>, AdditionalCheck<TBase> {

    constructor(
        public readonly data: TBase,
        private readonly currentResult: IntermediateResult) {
    }

    when(fn: Checker<TBase>, opts?: CheckOptions): AdditionalCheck<TBase> {
        opts = opts || {};
        if (this.currentResult.checkAndAdd(fn, this.data, opts)) {
            return new StoppedBaseValidator(this.data, this.currentResult);
        } else {
            return this;
        }
    }

    whenHas<TProp>(get: Get<TBase, TProp>, nameOverride?: string): PropertyCheck<TBase, TProp> {
        let derived = this.currentResult.derive(get, nameOverride);
        return new PropertyValidator<TBase, TProp>(get(this.data), this, derived);
    }

    whenFulfils(fn: InnerValidator<TBase>, opts?: CheckOptions): AdditionalCheck<TBase> {
        opts = opts || {};

        const oldErrors = this.currentResult.errorCount;
        fn(this);
        const newErrors = this.currentResult.errorCount;

        if (oldErrors !== newErrors && opts.fatal) {
            return new StoppedBaseValidator(this.data, this.currentResult);
        } else {
            return this;
        }
    }

    get result() {
        return this.currentResult.result;
    }

    get valid() {
        return this.currentResult.errorCount === 0;
    }

    get invalid() {
        return this.currentResult.errorCount !== 0;
    }

    and = this.when;
    andFulfils = this.whenFulfils;
    andHas = this.whenHas;

}

class PropertyValidator<TBase, TProp> implements PropertyCheck<TBase, TProp>, AdditionalPropertyCheck<TBase, TProp> {

    constructor(
        public readonly data: TProp,
        private readonly base: BaseValidator<TBase>,
        private readonly currentResult: IntermediateResult
    ) { }

    that(fn: Checker<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp> {
        opts = opts || {};
        if (this.currentResult.checkAndAdd(fn, this.data, opts)) {
            return new StoppedPropertyValidator<TBase, TProp>(this.data, this.base);
        } else {
            return this;
        }
    }

    thatFulfils(fn: InnerValidator<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp> {
        opts = opts || {};

        const oldErrors = this.currentResult.errorCount;
        const inner = new BaseValidator<TProp>(this.data, this.currentResult);
        fn(inner);
        const newErrors = this.currentResult.errorCount;

        if (oldErrors !== newErrors && opts.fatal) {
            return new StoppedPropertyValidator<TBase, TProp>(this.data, this.base);
        } else {
            return this;
        }
    }

    andThat = this.that;
    andThatFulfils = this.thatFulfils;

    andHas = this.base.andHas.bind(this.base);
    get result() { return this.base.result; }
    get valid() { return this.base.valid; }
    get invalid() { return this.base.invalid; }
}

class StoppedBaseValidator implements BaseCheck<any>, AdditionalCheck<any>, PropertyCheck<any, any>, AdditionalPropertyCheck<any, any> {

    constructor(
        readonly data: any,
        private readonly currentResult: IntermediateResult) {
    }

    when(fn: Checker<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    whenHas<TProp>(get: Get<any, any>): PropertyCheck<any, any> {
        return this;
    }

    whenFulfils(fn: InnerValidator<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    and(fn: Checker<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    andFulfils(fn: InnerValidator<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    andHas<TProp>(get: Get<any, any>): PropertyCheck<any, any> {
        return this;
    }

    that(fn: Checker<any>, opts?: CheckOptions): AdditionalPropertyCheck<any, any> {
        return this;
    }

    thatFulfils(fn: InnerValidator<any>): AdditionalPropertyCheck<any, any> {
        return this;
    }

    andThat(fn: Checker<any>, opts?: CheckOptions): AdditionalPropertyCheck<any, any> {
        return this;
    }

    andThatFulfils(fn: InnerValidator<any>): AdditionalPropertyCheck<any, any> {
        return this;
    }

    get result() {
        return this.currentResult.result;
    }

    get valid() {
        return this.currentResult.errorCount === 0;
    }

    get invalid() {
        return this.currentResult.errorCount !== 0;
    }
}

class StoppedPropertyValidator<TBase, TProp> implements BaseCheck<any>, AdditionalCheck<any>, PropertyCheck<any, any>, AdditionalPropertyCheck<TBase, TProp> {

    constructor(
        readonly data: TProp,
        private readonly base: BaseValidator<TBase>) {
    }

    when(fn: Checker<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    whenHas<TProp>(get: Get<any, any>): PropertyCheck<any, any> {
        return this;
    }

    whenFulfils(fn: InnerValidator<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    and(fn: Checker<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    andFulfils(fn: InnerValidator<any>, opts?: CheckOptions): AdditionalCheck<any> {
        return this;
    }

    that(fn: Checker<any>, opts?: CheckOptions): AdditionalPropertyCheck<any, any> {
        return this;
    }

    thatFulfils(fn: InnerValidator<any>): AdditionalPropertyCheck<any, any> {
        return this;
    }

    andThat(fn: Checker<TProp>, opts?: CheckOptions): AdditionalPropertyCheck<TBase, TProp> {
        return this;
    }

    andThatFulfils(fn: InnerValidator<TProp>): AdditionalPropertyCheck<TBase, TProp> {
        return this;
    }

    andHas = this.base.andHas.bind(this.base);
    get result() { return this.base.result; }
    get valid() { return this.base.valid; }
    get invalid() { return this.base.invalid; }
}
