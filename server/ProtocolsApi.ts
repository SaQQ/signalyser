import { Request, Response } from "express";

import { Protocol, ProtocolUpdate } from "./Contract";
import { translateCreate, translateUpdate, translateResult } from "./Utils/Translators";
import { Dispenser } from "./JobDispenser";

import * as db from "./Db";
import * as services from "./Services";
import * as validators from "./Validators";

export async function list(req: Request, res: Response) {
    let result = await db.Protocols.all();
    return translateResult(res, result);
}

export async function get(req: Request, res: Response) {
    const protoId = req.params.protocolId as string;

    if (!validators.validateId(res, protoId)) {
        return res;
    }
    let result = await db.Protocols.find(protoId);
    return translateResult(res, result);
}

export async function add(req: Request, res: Response) {
    const proto = req.body as Protocol;

    if (!validators.validateProtocol(res, proto)) {
        return res;
    }

    let result = await db.Protocols.add(proto);
    if (result.type === "ObjectCreated") {
        await Dispenser.reactToProtocolChange(proto.id);
    }
    return translateCreate(res, result);
}

export async function update(req: Request, res: Response) {
    const proto = req.body as ProtocolUpdate;
    const protoId = req.params.protocolId as string;

    if (!validators.validateProtocolUpdate(res, proto) ||
        !validators.validateId(res, protoId)) {
        return res;
    }
    let result = await services.Updater.update(protoId, proto);
    if (result.type === "ObjectUpdated") {
        await Dispenser.reactToProtocolChange(protoId);
    }
    return translateUpdate(res, result);
}
