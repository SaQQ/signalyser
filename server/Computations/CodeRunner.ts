import * as vm from "vm";

import { Protocol, ParameterDescriptor, FullSignal, FullExamination } from "../Contract";
import * as config from "../Config";

const EvaluatorRunner = new vm.Script("evaluate(exam)");

type EvaluateFunction = (exam: FullExamination) => Object;
interface ScriptExports {
    default?: EvaluateFunction;
    calculate?: EvaluateFunction;
    __esModule?: boolean;
}

export class CodeError extends Error {
    public readonly innerError: Error | undefined;

    constructor(message: string, innerError?: Error) {
        if (innerError !== undefined) {
            super(message + ", inner error: " + innerError.message);
        } else {
            super(message);
        }

        this.name = "CodeError";
        this.innerError = innerError;
    }
}

function safeCompileCode(code: string) {
    try {
        return new vm.Script(code, { timeout: config.RunnerTimeout });
    } catch (e) {
        throw new CodeError("Cannot compile the code", e);
    }
}

function getEvaluator(script: vm.Script): EvaluateFunction {
    const exports: ScriptExports = {};
    const sandbox = vm.createContext({ exports });

    try {
        script.runInContext(sandbox, { timeout: config.RunnerTimeout });
    } catch (e) {
        throw new CodeError("Cannot execute the code", e);
    }

    if (exports.default === undefined && exports.calculate === undefined) {
        throw new CodeError("The code does not have default export nor does not export calculate function");
    }
    let allowedLength = 1;
    if (exports.__esModule !== undefined) {
        if (typeof exports.__esModule !== "boolean") {
            throw new CodeError("The code exports __esModule but it is not a boolean value");
        }
        allowedLength = 2;
    }
    if (Object.getOwnPropertyNames(exports).length !== allowedLength) {
        throw new CodeError("The code exports something more than default function or calculate function");
    }

    // We can be sure that only one of the exports is available.
    const result = <EvaluateFunction>(exports.default || exports.calculate);
    if (typeof result !== "function") {
        throw new CodeError("The code must export a function, but it exports " + typeof result);
    }
    return result;
}

function sanitize(exam: FullExamination): FullExamination {
    return { ...exam, params: {} };
}

function evaluate(evaluate: EvaluateFunction, exam: FullExamination) {
    const ctx = vm.createContext({ evaluate, exam });
    try {
        return EvaluatorRunner.runInContext(ctx, { timeout: config.CalculationTimeout });
    } catch (e) {
        throw new CodeError("Execution failed", e);
    }
}

export interface ICodeRunner {
    run(examination: FullExamination, param: ParameterDescriptor): Object;
}

export class CodeRunner implements ICodeRunner {
    run(examination: FullExamination, param: ParameterDescriptor): Object {
        const script = safeCompileCode(param.compiledFormula);
        const evaluator = getEvaluator(script);
        const sanitized = sanitize(examination);
        return evaluate(evaluator, sanitized);
    }
}

