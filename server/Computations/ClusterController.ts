import { EventEmitter } from "events";
import * as cluster from "cluster";
import * as winston from "winston";
import * as os from "os";

import { AssignJobCommand, CancelJobCommand, JobFinished } from "./Commands";
import * as config from "../Config";

const WorkerScript = `${__dirname}/WorkerApp.js`;

export interface IClusterController {
    start(): Promise<void>;
    stop(): void;

    addJob(protoId: string, examId: string, paramName: string): void;
}

class RestartableWorker extends EventEmitter {
    private _isOnline = false;
    private isStarting = false;
    private worker: cluster.Worker;

    private restartAttempts = 0;
    private isShuttingDown = false;

    constructor(public readonly id: number) {
        super();
    }

    get isOnline() {
        return this._isOnline;
    }

    start() {
        if (this.isStarting || this._isOnline) {
            throw new Error("Worker is during start-up procedure or is already running");
        }

        this.isStarting = true;
        this.worker = cluster.fork();

        // We won't need these properties outside this promise, so I decided to
        // put it in the closure (also, we need to have exact references to some
        // of the object and this is the easiest way)
        return new Promise<void>((resolve, reject) => {
            // No online message, kill it with fire
            const tid = setTimeout(() => {
                winston.error("Worker %s don't want to start (no WorkerOnline message), killing", this.id);
                this.worker.kill();
                reject(new Error("Worker didn't send the WorkerOnline message"));
            }, config.WorkerSetupWait);

            const handleExit = (code: number, signal: string) => {
                winston.error("Worker %s exited before sending WorkerOnline message, code: %d, signal: %s", this.id, code, signal || "EXCEPTION");
                reject(new Error(`Worker exited before sending WorkerOnline message, code: ${code}, signal: ${signal}`));
            };

            let handleOnline: (msg: any) => void;
            handleOnline = (msg: any) => {
                if (msg && msg.type === "WorkerOnline") {
                    clearTimeout(tid);
                    this.worker.removeListener("message", handleOnline);
                    this.worker.removeListener("exit", handleExit);
                    this.configureOnlineWorker();
                    resolve();
                } else {
                    winston.error("Worker %s sent a message '%j', WorkerOnline expected, ignoring", this.id, msg);
                }
            };


            this.worker.on("exit", handleExit);
            this.worker.on("message", handleOnline);
        });
    }

    shutdown(force: boolean) {
        this.isShuttingDown = true;
        if (this.worker !== undefined) {
            winston.info("The cluster shuts down, killing worker %d", this.worker.id);
            this.worker.kill(force ? "SIGKILL" : "SIGTERM");
        }

        this.worker.removeAllListeners("exit");
        return new Promise<void>(resolve => {
            this.worker.on("exit", (code, signal) => {
                winston.info("Worker %s exited with code %d, as a response to signal %s (shutting down)", this.id, code, signal);
                resolve();
            });
        });
    }

    send(message: any) {
        if (!this._isOnline) {
            throw new Error("Worker is not running");
        }
        return this.worker.send(message);
    }

    private configureOnlineWorker() {
        winston.info("Worker %s is online", this.id);

        this.isStarting = false;
        this._isOnline = true;
        this.restartAttempts = 0;

        this.worker.on("exit", this.handleExit.bind(this));
        this.worker.on("message", this.handleMessage.bind(this));
        this.emit("online");
    }

    private handleMessage(message: any) {
        this.emit("message", message);
    }

    private handleExit(code: number, signal: string) {
        this._isOnline = false;

        if (!this.isShuttingDown) {
            winston.warn("Worker %s exited abruptly with code %d, as a response to signal %s, restarting", this.id, code, signal);
            this.tryRestart();
        }
    }

    private tryRestart() {
        this.emit("restarting");
        this.start().catch(e => {
            if (this.restartAttempts < config.WorkerRestartAttempts) {
                winston.warn("Worker %s don't want to restart, %d attempt, restarting", this.id, this.restartAttempts);
                this.restartAttempts++;
                this.tryRestart();
            } else {
                winston.error("Worker %s doesn't want to restart, attempts exceeded", this.id);
                this.emit("dead");
            }
        });
    }
}

class ComputationsWorker extends EventEmitter {
    private readonly worker: RestartableWorker;
    private _isLive = true;

    private currentItem: AssignJobCommand | null = null;
    private workQueue: AssignJobCommand[] = [];

    get id() {
        return this.worker.id;
    }

    get isLive() {
        return this._isLive;
    }

    constructor(id: number) {
        super();

        this.worker = new RestartableWorker(id);

        this.worker.on("message", this.handleMessage.bind(this));
        this.worker.on("dead", this.handleDead.bind(this));
        this.worker.on("online", this.dispatchNextJob.bind(this));
    }

    assignJob(cmd: AssignJobCommand) {
        if (!this.isLive) {
            throw new Error("Cannot assign job to dead worker");
        }
        winston.verbose("Queueing job %s to worker %d", cmd.refNumber, this.id);
        this.workQueue.push(cmd);
        this.dispatchNextJob();
    }

    cancelJob(cmd: CancelJobCommand) {
        winston.verbose("Canceling job %s in worker %d", cmd.refNumber, this.id);
        let idx;
        if (this.currentItem !== null && this.currentItem.refNumber === cmd.refNumber) {
            winston.verbose("Canceling current job %s, worker %d", cmd.refNumber, this.id);
            this.worker.send(cmd);
        } else if ((idx = this.workQueue.findIndex(w => w.refNumber === cmd.refNumber)) !== -1) {
            winston.verbose("Removind queued job %s from worker %d", cmd.refNumber, this.id);
            this.workQueue.splice(idx, 1);
        } else {
            winston.warn("Job %s is not assigned to worker %d", cmd.refNumber, this.id);
        }
    }

    start() {
        return this.worker.start();
    }

    shutdown(force: boolean) {
        return this.worker.shutdown(force);
    }

    private dispatchNextJob() {
        if (!this.worker.isOnline) {
            winston.warn("Worker %d is offline, cannot dispatch job", this.id);
        } else if (this.currentItem === null) {
            const nextJob = this.workQueue.shift();
            if (nextJob === undefined) {
                winston.verbose("Worker %d is free", this.id);
            } else {
                winston.info("Sending compute job %s to worker %d (param %s of examination %s of protocol %s)",
                    nextJob.refNumber, this.id, nextJob.paramName, nextJob.examId, nextJob.protoId);
                this.currentItem = nextJob;
                this.worker.send(nextJob);
            }
        }
    }

    private handleMessage(message: any) {
        if (message && message.type === "JobFinished" &&
            message.refNumber !== undefined && message.refNumber !== "") {
            const msg: JobFinished = message;
            if (this.currentItem === null) {
                winston.error("Worker %d notified of finishing job %s, but we are not tracking any job", this.id, msg.refNumber);
            } else if (this.currentItem.refNumber !== msg.refNumber) {
                winston.error("We are tracking job %d, but worker %d sent JobFinished for job %s", this.currentItem.refNumber, this.id, msg.refNumber);

                const idx = this.workQueue.findIndex(c => c.refNumber === msg.refNumber);
                if (idx < -1) {
                    this.workQueue[idx] = this.currentItem;
                }
                this.currentItem = null;
            } else {
                winston.verbose("Worker %d finished job %s", this.id, msg.refNumber);
                this.currentItem = null;
                this.dispatchNextJob();
            }
            this.emit("finished", msg.refNumber);
        } else {
            winston.warn("Worker %d has sent invalid message: '%j', ignoring", this.id, message);
        }
    }

    private handleDead() {
        if (this.currentItem !== null) {
            winston.error("Worker %d is dead, probable reason is the job %s (param %s of exam %s of proto %s)",
                this.id, this.currentItem.refNumber, this.currentItem.paramName, this.currentItem.examId, this.currentItem.protoId);
        }

        const savedQueue = this.workQueue;
        this._isLive = false;
        this.workQueue = [];
        this.emit("dead", this, savedQueue);
    }
}

export class ClusterController implements IClusterController {

    private readonly workers: ComputationsWorker[];
    private nextWorker = 0;
    private liveWorkers = 0;

    private workList: {
        protoId: string;
        examId: string;
        paramName: string;
        refNumber: string;
        worker: number;
    }[] = [];
    private nextRef = 0;


    constructor() {
        this.workers = os.cpus().map((c, i) => new ComputationsWorker(i));
    }

    start() {
        winston.info("Starting cluster controller");
        cluster.setupMaster({ exec: WorkerScript });

        winston.verbose("Starting workers");

        return Promise.all(this.workers.map(w => w.start()))
            .then(() => { this.setupTracking(); })
            .catch(e => { // Some node didn't start up, killing the rest
                winston.error("Cannot start cluster controller, ensuring all are dead");
                this.workers.forEach(w => w.shutdown(true));
                return Promise.reject(e);
            });
    }

    stop() {
        let shutdownPromises = this.workers.map(w => w.shutdown(true));
        winston.info("Cluster controller shat down");
        return Promise.all(shutdownPromises);
    }

    addJob(protoId: string, examId: string, paramName: string) {
        const refNumber = this.nextRef.toString();
        this.nextRef++;

        winston.info("Adding new job %s - calculating param %s of exam %s of proto %s", refNumber, paramName, examId, protoId);
        this.cancelJob(protoId, examId, paramName);

        const worker = this.dispatchJob({
            type: "AssignJobCommand",
            protoId, examId, paramName, refNumber
        });
        this.workList.push({ protoId, examId, paramName, refNumber, worker });
    }

    private cancelJob(protoId: string, examId: string, paramName: string) {
        const itemIdx = this.workList.findIndex(w =>
            w.protoId === protoId && w.examId === examId && w.paramName === paramName);

        if (itemIdx > -1) {
            const item = this.workList[itemIdx];
            winston.info("Canceling job %s (param %s of exam %s of proto %s)", item.refNumber, paramName, examId, protoId);
            this.workers[item.worker].cancelJob({
                type: "CancelJobCommand",
                refNumber: item.refNumber
            });
            this.workList.splice(itemIdx, 1);
        }
    }

    private dispatchJob(cmd: AssignJobCommand) {
        while (!this.workers[this.nextWorker].isLive) {
            this.nextWorker = (this.nextWorker + 1) % this.workers.length;
        }
        const workerIdx = this.nextWorker;
        this.nextWorker = (this.nextWorker + 1) % this.workers.length;

        const worker = this.workers[workerIdx];
        winston.verbose("Dispatching job %s to worker %d (param %s of exam %s of proto %s)",
            cmd.refNumber, worker.id, cmd.paramName, cmd.examId, cmd.protoId);
        worker.assignJob(cmd);

        return workerIdx;
    }

    private setupTracking() {
        this.liveWorkers = this.workers.length;
        this.workers.forEach(w => {
            w.on("dead", this.handleDeadNode.bind(this));
            w.on("finished", this.handleFinishedJob.bind(this));
        });
    }

    private handleDeadNode(worker: ComputationsWorker, leftItems: AssignJobCommand[]) {
        this.liveWorkers--;
        if (this.liveWorkers > 0) {
            winston.warn("Worker %d has died, distributing %d items to %d living workers",
                worker.id, leftItems.length, this.liveWorkers);

            leftItems.forEach(i => this.dispatchJob(i));
        } else {
            winston.error("Worker %d has died, no more workers left, shutting down", worker.id);
            process.exit(1);
        }
    }

    private handleFinishedJob(refNumber: string) {
        const idx = this.workList.findIndex(w => w.refNumber === refNumber);
        this.workList.splice(idx, 1);
    }
}
