import { SignalData } from "../Contract";


// The format:
//  4B - Uint32 - N = Samples dimension
//  4B - Uint32 - C = Samples count per dimension
//  4B * C - Float32 - time data
//  4B * C * N - Float32 - samples (consecutive buffers per dimension)

export interface ISignalParser {
    validate(data: Buffer): boolean;
    parse(data: Buffer): SignalData;
}

export class SignalParser implements ISignalParser {
    validate(data: Buffer) {
        if (data.length < Uint32Array.BYTES_PER_ELEMENT * 2) {
            return false;
        }
        const header = new Uint32Array(data.buffer, data.byteOffset, 2);
        const dim = header[0];
        const len = header[1];

        if (data.length !== Uint32Array.BYTES_PER_ELEMENT * 2 +
            (len + len * dim) * Float32Array.BYTES_PER_ELEMENT) {
            return false;
        }
        return true;
    }

    parse(data: Buffer): SignalData {
        let offset = data.byteOffset;

        const header = new Uint32Array(data.buffer, data.byteOffset, 2);
        const dim = header[0];
        const len = header[1];

        offset += 2 * Uint32Array.BYTES_PER_ELEMENT;

        const timeData = new Float32Array(data.buffer, offset, len);
        offset += len * Float32Array.BYTES_PER_ELEMENT;

        const seriesData: Float32Array[] = [];
        for (let i = 0; i < dim; i++) {
            seriesData.push(new Float32Array(data.buffer, offset, len));
            offset += len * Float32Array.BYTES_PER_ELEMENT;
        }

        return { timeData, seriesData };
    }
}
