import * as cluster from "cluster";
import * as winston from "winston";
import * as mongo from "mongodb";

import { Worker } from "./Worker";
import { SignalParser } from "./SignalParser";
import { CodeRunner } from "./CodeRunner";
import { ExaminationReader } from "./ExaminationReader";
import { JobControl } from "./JobControl";
import { AssignJobCommand, CancelJobCommand, JobFinished, WorkerOnline } from "./Commands";

import * as db from "../Db/Mongo";

import "../Logging";

const runner = new CodeRunner();
const parser = new SignalParser();
const examReader = new ExaminationReader(db.Examinations, db.Protocols, db.Signals, parser);
const worker = new Worker(runner, examReader);
const jobControl = new JobControl(worker, db.Examinations);

function attachControl() {
    winston.verbose("Connected to the database, attaching required functionality");

    jobControl.jobFinished = m => cluster.worker.send(m);
    cluster.worker.on("message", parseMessage);
    cluster.worker.on("exit", code => winston.warn("We are being shut-down with code %d", code));
}

function notifyOnline() {
    const msg: WorkerOnline = { type: "WorkerOnline" };
    cluster.worker.send(msg);
}

function parseMessage(msg: any) {
    if (!validateMessage(msg)) {
        winston.error("Invalid message '%j' received", msg);
    } else {
        // TS type system can't handle any -> AssignJobCommand | CancelJobCommand -> AssignJobCommand
        // conversion, we must help it.
        const parsed: AssignJobCommand | CancelJobCommand = msg;
        if (parsed.type === "AssignJobCommand") {
            winston.debug("AssignJobCommand received, dispatching");
            jobControl.assignJob(parsed);
        } else {
            winston.debug("CancelJobCommand received, dispatching");
            jobControl.cancelJob(parsed);
        }
    }
}

function validateMessage(msg: any): msg is AssignJobCommand | CancelJobCommand {
    if (msg && msg.type) {
        const parsed: AssignJobCommand | CancelJobCommand = msg;
        if (parsed.type === "CancelJobCommand") {
            return parsed.refNumber !== undefined && parsed.refNumber !== "";
        } else if (parsed.type === "AssignJobCommand") {
            return parsed.protoId !== undefined && parsed.protoId !== "" &&
                parsed.examId !== undefined && parsed.examId !== "" &&
                parsed.paramName !== undefined && parsed.paramName !== "" &&
                parsed.refNumber !== undefined && parsed.refNumber !== "" &&
                mongo.ObjectID.isValid(parsed.protoId) &&
                mongo.ObjectID.isValid(parsed.examId);
        }
    }
    return false;
}

function reportError(e: any) {
    winston.error("Cannot connect to database", e);
    process.exit(2);
}

winston.info("Starting worker %s", cluster.worker.id);
db.Client.connect().then(attachControl).then(notifyOnline).catch(reportError);
