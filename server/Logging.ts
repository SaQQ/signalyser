import * as cluster from "cluster";
import * as winston from "winston";

import * as config from "./Config";
import * as cr from "./Computations/CodeRunner";

// [Work N].len = [Master].len, that's why we use `Work` instead of `Worker`
const ProcLabel = cluster.isWorker ? "[Work " + cluster.worker.id + "]" : "[Master]";

function formatter(opts: any) {
    let msg = ProcLabel +
        `[${new Date().toISOString()}] ` +
        opts.level + ": " +
        opts.message + ".";

    if (opts.meta && opts.meta.stack) {
        msg += " " + opts.meta.stack;
    }

    return msg;
}

winston.configure({
    transports: [new (winston.transports.Console)({
        timestamp: true,
        showLevel: true,
        colorize: process.env.NODE_ENV === "development" ? true : false,
        formatter
    })],
    level: config.LogLevel
});
