import { ObjectID } from "mongodb";

import { DbProtocol, DbExamination } from "./Types";

import * as contract from "../Contract";

export class Protocol {

    static readonly descriptionProjection = { _id: true, name: true };

    static dbToDescription(proto: DbProtocol): contract.ProtocolDescription {
        return { id: proto._id.toString(), name: proto.name };
    }

    static dbToApi(proto: DbProtocol): contract.Protocol {
        let { _id, ...rest } = proto;
        return { ...rest, id: _id.toString() };
    }

    static apiToDb(proto: contract.Protocol, protoId?: string): DbProtocol {
        let { id, ...rest } = proto;
        return { ...rest, _id: new ObjectID(protoId) };
    }

}

export class Examinations {

    static readonly descriptionProjection = { _id: true, tags: true, params: true };

    static dbToDescription(exam: DbExamination): contract.ExaminationDescription {
        return { id: exam._id.toString(), tags: exam.tags, params: exam.params };
    }

    static dbToApi(exam: DbExamination, signals: contract.AvailableSignal[]): contract.Examination {
        let {_id, ...rest} = exam;
        return { ...rest, id: _id.toString(), signals };
    }

    static apiToDb(exam: contract.ExaminationDescription, examId?: string): DbExamination {
        let {id, ...rest} = exam;
        return { ...rest, _id: new ObjectID(examId) };
    }

}

interface SignalFile {
    metadata: {
        protoId: string;
        examId: string;
        stage: string;
        signal: string;
    };
}

export class Signals {

    static readonly descriptionProjection = { _id: true, metadata: true };

    static dbToDescription(file: SignalFile): contract.AvailableSignal {
        return {
            stage: file.metadata.stage,
            signal: file.metadata.signal
        };
    }

}
