import * as mongo from "mongodb";
import * as winston from "winston";

import { DbProtocol } from "./Types";
import { Protocol, ProtocolDescription } from "../Contract";

import * as db from "./DbClient";
import * as mappers from "./Mappers";

export interface IProtocolsClient {
    all(): Promise<db.ReadResult<ProtocolDescription[]>>;
    find(id: string): Promise<db.ReadResult<Protocol>>;
    add(proto: Protocol): Promise<db.CreateResult>;
    update(id: string, proto: Protocol): Promise<db.UpdateResult>;
}

export class ProtocolsClient implements IProtocolsClient {

    constructor(private client: db.IDbClient) { }

    async all() {
        return await db.read(await this.client.protocols
            .find({}, mappers.Protocol.descriptionProjection)
            .sort({ name: 1 })
            .map(mappers.Protocol.dbToDescription)
            .toArray());
    }

    async find(id: string) {
        let _id = mongo.ObjectID.createFromHexString(id);
        let proto = await this.client.protocols.findOne({ _id });
        if (proto === null) {
            return db.notFound();
        } else {
            return db.read(mappers.Protocol.dbToApi(proto));
        }
    }

    async add(proto: Protocol): Promise<db.CreateResult> {
        let dbProto = mappers.Protocol.apiToDb(proto);
        try {
            let result = await this.client.protocols.insertOne(dbProto);
            winston.info("New protocol (id: %s) inserted, creating collection", result.insertedId);
            await this.client.createExaminationsCollection(dbProto._id.toHexString());
            return db.created(result);
        } catch (e) {
            winston.warn("Cannot insert new protocol", e);
            return db.internalError();
        }
    }

    async update(id: string, proto: Protocol): Promise<db.UpdateResult> {
        let dbProto = mappers.Protocol.apiToDb(proto, id);
        try {
            let result = await this.client.protocols.updateOne({ _id: dbProto._id }, dbProto);
            if (result.matchedCount === 0) {
                winston.verbose("Protocol with id %s not found, cannot update", id);
                return db.notFound();
            } else {
                winston.info("Protocol with id %s updated", id);
                return db.updated(dbProto._id);
            }
        } catch (e) {
            winston.warn("Cannot update protocol %s", id, e);
            return db.internalError();
        }
    }

}

