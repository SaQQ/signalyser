import * as mongo from "mongodb";
import * as stream from "stream";
import * as winston from "winston";

import { SignalMimeType, SignalData, AvailableSignal } from "../Contract";

import * as db from "./DbClient";
import * as mapper from "./Mappers";

export interface SignalDescription {
    protoId: string;
    examId: string;
    stage: string;
    signal: string;
}

export interface SignalWriteRequest extends SignalDescription {
    data: stream.Readable;
}

export interface SignalReadRequest extends SignalDescription {
    output: stream.Writable;
}

export interface ISignalsClient {
    list(protoId: string, examId: string): Promise<db.ReadResult<AvailableSignal[]>>;
    download(signal: SignalReadRequest): Promise<db.ReadResult<undefined>>;
    upload(data: SignalWriteRequest): Promise<db.CreateResult>;

    rename(protoId: string, stages: db.NamedUpdate[], signals: db.NamedUpdate[]): Promise<db.CollectionUpdateResult>;
}

export class SignalsClient implements ISignalsClient {

    private signals: mongo.GridFSBucket;

    constructor(private readonly client: db.IDbClient) {
        client.on("connected", () => {
            this.signals = client.signals;
        });
    }

    async list(protoId: string, examId: string) {
        return db.read(await this.signals
            .find({ "metadata.examId": examId })
            .project(mapper.Signals.descriptionProjection)
            .map(mapper.Signals.dbToDescription)
            .toArray());
    }

    download(request: SignalReadRequest) {
        return this.getFileId(request).then(file => {
            if (file === null) {
                return db.notFound();
            } else {
                return new Promise<db.ReadResult<undefined>>((resolve, reject) => {
                    this.signals.openDownloadStream(file)
                        .on("error", err => {
                            if ((err as any).code === "ENOENT") {
                                winston.verbose("Cannot read signal for protocol %s, exam %s - %s/%s - it does not exist",
                                    request.protoId, request.examId, request.stage, request.signal);
                                resolve(db.notFound());
                            } else {
                                winston.error("Cannot read signal for protocol %s, exam %s - %s/%s - exception was thrown: %s",
                                    request.protoId, request.examId, request.stage, request.signal, err);
                                resolve(db.internalError());
                            }
                        })
                        .pipe(request.output)
                        .on("error", err => {
                            winston.error("Cannot read signal for protocol %s, exam %s - %s/%s - exception was thrown: %s",
                                request.protoId, request.examId, request.stage, request.signal, err);
                            resolve(db.internalError());
                        }).on("finish", () => {
                            resolve(db.read(undefined));
                        });
                });
            }
        });
    }

    async upload(request: SignalWriteRequest) {
        try {
            // TODO: preserve old data?
            await this.deleteExisting(request);
            let id = await this.addSignal(request);
            winston.info("New signal for protocol %s, exam %s - %s/%s saved",
                request.protoId, request.examId, request.stage, request.signal);
            return db.created(id);
        } catch (e) {
            winston.error("Cannot upload signal file", e);
            return db.internalError();
        }
    }

    async rename(protoId: string, stages: db.NamedUpdate[], signals: db.NamedUpdate[]) {
        try {
            const files = this.client.signalFiles;
            for (const stage of stages) {
                await files.updateMany(
                    { "metadata.protoId": protoId, "metadata.stage": stage.oldName },
                    { "$set": { "metadata.stage": stage.newName } });
                winston.debug("Files from stage '%s' renamed to stage '%s', protocol %s", stage.oldName, stage.newName, protoId);
            }

            for (const signal of signals) {
                await files.updateMany(
                    { "metadata.protoId": protoId, "metadata.signal": signal.oldName },
                    { "$set": { "metadata.signal": signal.newName } });
                winston.debug("Files from signal '%s' renamed to signal '%s', protocol %s", signal.oldName, signal.newName, protoId);
            }
            return db.collectionUpdated();
        } catch (e) {
            winston.error("Cannot update stages/signals for signals of protocol %s, Mongo returned an error", protoId, e);
            return db.internalError();
        }
    }

    private async deleteExisting(desc: SignalDescription) {
        let file = await this.getFileId(desc);
        if (file !== null) {
            winston.verbose("Deleting existing signal file %s", file.toString());
            this.signals.delete(file);
        }
    }

    private addSignal(request: SignalWriteRequest) {
        return new Promise<mongo.ObjectID>((resolve, reject) => {
            let { data, ...desc } = request;

            const name = new mongo.ObjectID().toString();
            let stream = this.signals.openUploadStream(name, {
                contentType: SignalMimeType,
                metadata: desc
            });
            let id = (stream as any).id as mongo.ObjectID;
            data
                .pipe(stream)
                .on("error", (err) => reject(err))
                .on("finish", () => resolve(id));
        });
    }

    private getFileId(desc: SignalDescription): Promise<mongo.ObjectID | null> {
        return this.client.signalFiles.findOne({
            "metadata.protoId": desc.protoId,
            "metadata.examId": desc.examId,
            "metadata.stage": desc.stage,
            "metadata.signal": desc.signal
        }, { fields: { _id: true } })
            .then(r => r === null ? null : r._id);
    }
}
