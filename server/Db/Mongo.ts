import { IDbClient, DbClient } from "./DbClient";
import { IExaminationsClient, ExaminationsClient } from "./ExaminationsClient";
import { IProtocolsClient, ProtocolsClient } from "./ProtocolsClient";
import { ISignalsClient, SignalsClient } from "./SignalsClient";

export const Client: IDbClient = new DbClient();
export const Protocols: IProtocolsClient = new ProtocolsClient(Client);
export const Signals: ISignalsClient = new SignalsClient(Client);
export const Examinations: IExaminationsClient = new ExaminationsClient(Client, Signals);
