import * as process from "process";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as winston from "winston";

import * as config from "./Config";
import * as db from "./Db";
import * as protoApi from "./ProtocolsApi";
import * as examApi from "./ExaminationsApi";
import * as signalsApi from "./SignalsApi";
import { Dispenser } from "./JobDispenser";

import "./Logging";

const app = express()
    .use(bodyParser.json());

app.get("/", (req, res) => res.sendStatus(200));

app.route("/protocols")
    .get(protoApi.list)
    .post(protoApi.add);

app.route("/protocols/:protocolId")
    .get(protoApi.get)
    .put(protoApi.update);

app.route("/protocols/:protocolId/examinations")
    .post(examApi.list)
    .put(examApi.add);

app.route("/protocols/:protocolId/examinations/recompute")
    .post(examApi.recomputeProtocol);

app.route("/protocols/:protocolId/examinations/:examId")
    .get(examApi.get)
    .put(examApi.update);

app.route("/protocols/:protocolId/examinations/:examId/recompute")
    .post(examApi.recompute);

app.route("/protocols/:protocolId/examinations/:examId/signals/:stage/:signal")
    .get(signalsApi.download)
    .put(signalsApi.upload);

let server: http.Server;

function cleanupAndStop() {
    winston.info("Stopping the server");

    let closeCluster = Dispenser.stop();
    let closeApp = new Promise(r => server.close(r));
    let closeDb = db.Client.disconnect();
    Promise.all([closeCluster, closeApp, closeDb])
        .then(() => process.exit(0))
        .catch(e => {
            winston.error("Cannot properly close the server/db", e);
            process.exit(1);
        });
}

async function startApp() {
    try {
        await db.Client.connect();
    } catch (e) {
        winston.error("Cannot open connection to MongoDb", e);
        process.exit(1);
        return;
    }

    try {
        await Dispenser.start();
    } catch (e) {
        winston.error("Cannot start cluster controller", e);
        process.exit(2);
        return;
    }
    server = app.listen(config.ExposePort, () => winston.info(`Signalyser storage listening on port ${config.ExposePort}`));
    process.once("SIGTERM", cleanupAndStop);
    if (process.env.NODE_ENV !== "development") {
        process.once("SIGINT", cleanupAndStop);
    }
}

startApp();
