import { should } from "../Startup";

import { JobControl, } from "../../server/Computations/JobControl";
import { AssignJobCommand, CancelJobCommand, JobFinished } from "../../server/Computations/Commands";

import * as stub from "./StubData";

describe("Computations/JobControl", function () {
    this.timeout(100);

    const worker = new stub.StubWorker();
    const client = new stub.StubParamsExaminationsClient();

    let jobControl: JobControl;

    beforeEach(function () {
        worker.reset();
        client.reset();
        jobControl = new JobControl(worker, client);
    });

    it("schedules computation and fires jobFinished handler", function () {
        return test(() => { }, run);
    });

    it("notifies about success of correct computation", function () {
        const refNum = "test-ref";
        return test(
            r => {
                r.canceled.should.be.false;
                r.refNumber.should.be.equal(refNum);
            },
            () => run(refNum)
        );
    });

    it("schedules the computation to worker", function () {
        return test(
            () => {
                worker.called.should.be.true;
                worker.protoId.should.be.equal(stub.protoId);
                worker.examId.should.be.equal(stub.examId);
                worker.paramName.should.be.equal(stub.paramName);
            },
            run
        );
    });

    it("saves the result", function () {
        const result = { sampleDat: "this-is-spartaaaa!" };
        return test(
            () => {
                client.called.should.be.true;
                client.protoId.should.be.equal(stub.protoId);
                client.examId.should.be.equal(stub.examId);
                client.paramData.should.be.deep.equal([[stub.paramName, result]]);
            },
            () => runInternal("ref", true, result)
        );
    });

    it("notifies about finish if the job is canceled", function () {
        const ref = "sample-ref";
        worker.delay = 10;
        const promise = test(
            () => {

            }, () => run(ref));
        jobControl.cancelJob({ type: "CancelJobCommand", refNumber: ref });
        return promise;
    });

    it("does not save the result if it gets canceled before finish", function () {
        const ref = "sample-ref";
        worker.delay = 10;
        const promise = test(
            () => { client.called.should.be.false; },
            () => run(ref)
        );
        jobControl.cancelJob({ type: "CancelJobCommand", refNumber: ref });
        return promise;
    });

    it("notifies about finish if the computation fails", function () {
        return test(
            r => { r.canceled.should.be.false; },
            runFailedComputation
        );
    });

    it("notifies about finish if the update fails", function () {
        return test(
            r => { r.canceled.should.be.false; },
            runFailedUpdate
        );
    });

    it("correctly passes arguments through the flow if multiple jobs are running", function () {
        const job = { protoId: stub.protoId, examId: stub.examId };
        const data = [
            { paramName: "p1", refNumber: "1", result: 1 },
            { paramName: "p2", refNumber: "2", result: 2 }];
        worker.result = (_, __, n) => (<any>data.find(d => d.paramName === n)).result;
        worker.delay = 10;
        client.success = true;

        const promise = new Promise<JobFinished>((resolve, reject) => {
            jobControl.jobFinished = e => resolve(e);
        }).then(e => {
            try {
                const mm = data.map(d => [d.paramName, d.result]);
                client.paramData.should.have.deep.members(mm);
                return Promise.resolve();
            } catch (e) {
                return Promise.reject(e);
            }
        });
        data.forEach(j => jobControl.assignJob({
            type: "AssignJobCommand",
            protoId: job.protoId,
            examId: job.examId,
            paramName: j.paramName,
            refNumber: j.refNumber,
        }));
        return promise;
    });

    function test(tst: (event: JobFinished) => void, run: () => void) {
        let promise = new Promise<void>((resolve, reject) => {
            jobControl.jobFinished = e => {
                try {
                    tst(e);
                    resolve();
                } catch (e) {
                    reject(e);
                }
            };
        });
        run();
        return promise;
    }

    function runInternal(refNumber?: string, success?: boolean, result?: Object, paramName?: string) {
        worker.result = result && (() => result);
        client.success = success !== undefined ? success : true;
        jobControl.assignJob({
            type: "AssignJobCommand",
            protoId: stub.protoId,
            examId: stub.examId,
            paramName: paramName || stub.paramName,
            refNumber: refNumber || "sample-ref-number"
        });
    }

    const run = (r?: string) => runInternal(r, true, {});
    const runFailedComputation = (r?: string) => runInternal(r, true, undefined);
    const runFailedUpdate = (r?: string) => runInternal(r, false, {});

});
