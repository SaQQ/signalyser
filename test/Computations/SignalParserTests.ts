import { should } from "../Startup";

import { SignalParser } from "../../server/Computations/SignalParser";

import * as stub from "./StubData";

describe("Computation/SignalParser", function () {
    const parser = new SignalParser();

    describe("#validate", function () {

        it("returns false when buffer is empty", function () {
            validate([]).should.be.false;
        });

        it("returns false when the header fields are not fully present", function () {
            validate([0x0]).should.be.false;
        });

        it("returns false when the data is not present", function () {
            semiValidate(1, 1, 0).should.be.false;
        });

        it("returns false when the data is smaller than required", function () {
            semiValidate(1, 5, 2).should.be.false;
        });

        it("returns false when the data is too small for specified dimensions", function () {
            semiValidate(2, 1, 1).should.be.false;
        });

        it("returns false when the data is too small for specified dimensions and longer data", function () {
            semiValidate(2, 5, 5).should.be.false;
        });

        it("returns false when the data is too big", function () {
            semiValidate(2, 5, 10).should.be.false;
        });

        it("returns true when the data is of valid length, single dimension", function () {
            semiValidate(1, 10, 20).should.be.true;
        });

        it("returns true when the data is of valid length, multiple dimensions", function () {
            semiValidate(5, 10, 60).should.be.true;
        });

        it("returns true when there are no dimensions but time data is present", function () {
            semiValidate(0, 10, 10).should.be.true;
        });

        it("returns true when there are dimensions, but with zero length", function () {
            semiValidate(5, 0, 0).should.be.true;
        });

        function validate(data: number[]) {
            return parser.validate(Buffer.from(data));
        }

        function semiValidate(dim: number, len: number, dataLen: number) {
            const header = new Uint32Array([dim, len]).buffer;
            const data = new Float32Array(dataLen).buffer;
            const buffer = Buffer.concat([header, data].map(d => Buffer.from(d)));
            return parser.validate(buffer);
        }

    });

    describe("#parse", function () {

        it("uses `Float32Array` as time data", function () {
            parse(0, 1, [1], []).timeData.should.be.instanceOf(Float32Array);
        });

        it("uses `Array` to store each dimension", function () {
            parse(5, 1, [1], [[1], [2], [3], [4], [5]]).seriesData.should.be.instanceOf(Array);
        });

        it("uses `Float32Array` to store dimension data", function () {
            const result = parse(5, 1, [1], [[1], [2], [3], [4], [5]]).seriesData;
            result.every(e => e instanceof Float32Array).should.be.true;
        });

        // Typed arrays are represnted as objects with indexes being string keys
        // and values being values, which breaks `equal`. Here we can be sure that
        // they are `Float32Array`s (prev tests), so we just convert them.

        it("parses time data", function () {
            const time = [0, 1, 2, 3, 4];
            const result = parse(0, 5, time, []).timeData;
            Array.from(result).should.be.deep.equal(time);
        });

        it("parses single dimensions", function () {
            const data = [0, 1, 2, 3, 4];
            const result = parse(1, 5, data, [data]).seriesData;
            Array.from(result[0]).should.be.deep.equal(data);
        });

        it("parses multiple dimensions", function () {
            const data1 = [0, 1, 2, 3, 4];
            const data2 = [9, 8, 7, 4, 5];
            const result = parse(2, 5, data1, [data1, data2]).seriesData;
            result.map(r => Array.from(r)).should.be.deep.eq([data1, data2]);
        });


        function parse(dim: number, len: number, time: number[], series: number[][]) {
            const header = new Uint32Array([dim, len]).buffer;
            const timeData = new Float32Array(time).buffer;
            const seriesData = series.map(s => new Float32Array(s).buffer);
            const buffer = Buffer.concat([header, timeData].concat(seriesData).map(d => Buffer.from(d)));
            return parser.parse(buffer);
        }

    });

});
