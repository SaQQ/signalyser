import {
    Protocol,
    ParameterDescriptor, TagDescriptor, SignalDescriptor, StageDescriptor,
    AttributesCollection, TagValue, FullExamination, Examination,
    AvailableSignal, SignalData, FullSignal
} from "../../server/Contract";

import { Errored, Computed } from "../../server/Computations/Worker";
import { IExaminationReader, ReadExamination } from "../../server/Computations/ExaminationReader";
import { CodeError, ICodeRunner } from "../../server/Computations/CodeRunner";
import { ISignalParser } from "../../server/Computations/SignalParser";
import { IWorker } from "../../server/Computations/Worker";
import { IExaminationsClient } from "../../server/Db/ExaminationsClient";
import { IProtocolsClient } from "../../server/Db/ProtocolsClient";
import { ISignalsClient, SignalReadRequest, SignalDescription } from "../../server/Db/SignalsClient";
import { ReadResult, UpdateResult, ObjectUpdated, ErrorType, read, error } from "../../server/Db/DbClient";

export const protoId = "sample-proto-id";
export const examId = "sample-exam-id";
export const paramName = "param";
export const tagName = "sampleTag";

export const signal: SignalDescriptor = { name: "Signal1" };
export const stage: StageDescriptor = { name: "Stage1" };
export const tag: TagDescriptor = { name: tagName };
export const param: ParameterDescriptor = { name: paramName, formula: "", compiledFormula: "" };

export const signalDescriptions: AvailableSignal[] = [
    {
        stage: "stage-a",
        signal: "signal-a"
    },
    {
        stage: "stage-b",
        signal: "signal-b"
    }
];
export const protocol = makeProtocol(param);
export const realSignal = makeSignal();
export const fullExamination = makeFullExamination(param);
export const readExamination = makeReadExamination(param);
export const successfulJobResult: Computed = { type: "Computed", data: { sampleData: "sample" } };

export function makeSignalData(): SignalData {
    return {
        timeData: [0, 1, 2],
        seriesData: [[0, 1, 2], [0, 2, 4]]
    };
}
export function makeSignal(): FullSignal {
    return {
        signal: signal.name,
        stage: stage.name,
        data: makeSignalData()
    };
}

export function makeProtocol(param: ParameterDescriptor): Protocol {
    return {
        id: protoId,
        name: "Tests",
        signals: [signal],
        stages: [stage],
        tags: [tag],
        params: [param]
    };
}
export function makeFullExamination(param: ParameterDescriptor): FullExamination {
    return {
        id: examId,
        tags: {
            [tagName]: "Sample value"
        },
        params: {
            [paramName]: "sample value"
        },
        signals: [realSignal]
    };
}

export function makeReadExamination(param: ParameterDescriptor): ReadExamination {
    return {
        id: examId,
        protocol: makeProtocol(param),
        tags: {
            [tagName]: "Sample value"
        },
        params: {
            [paramName]: "sample value"
        },
        signals: [realSignal]
    };
}

export function makeExamination(): Examination {
    return {
        id: examId,
        tags: {
            [tagName]: "Sample value"
        },
        params: {
            [param.name]: "sample value"
        },
        signals: signalDescriptions
    };
}

export class ComputationsRunner implements ICodeRunner {

    private _called: boolean = false;
    private _examination: FullExamination;
    private _param: ParameterDescriptor;

    public error?: CodeError;
    public result?: Object;

    get called() {
        return this._called;
    }

    get examination() {
        return this._examination;
    }

    get param() {
        return this._param;
    }

    run(examination: FullExamination, param: ParameterDescriptor): Object {
        this._called = true;
        this._examination = examination;
        this._param = param;

        if (this.result !== undefined) {
            return this.result;
        } else {
            throw this.error;
        }
    }

    reset() {
        this._called = false;
        this.result = undefined;
        this.error = undefined;
    }

}

export class ExaminationReader implements IExaminationReader {

    private _called: boolean = false;
    private _protoId: string;
    private _examId: string;

    public result?: ReadExamination;
    public error?: Error;

    get called() {
        return this._called;
    }

    get protoId() {
        return this._protoId;
    }

    get examId() {
        return this._examId;
    }

    download(protoId: string, examId: string): Promise<ReadExamination> {
        this._called = true;
        this._protoId = protoId;
        this._examId = examId;

        if (this.result !== undefined) {
            return Promise.resolve(this.result);
        } else {
            return Promise.reject(this.error);
        }
    }

    reset() {
        this._called = false;
        this.result = undefined;
        this.error = undefined;
    }
}

export class StubExaminationsClient implements IExaminationsClient {

    private _called: boolean = false;
    private _protoId: string;
    private _examId: string;

    public result?: Examination;

    get called() {
        return this._called;
    }

    get protoId() {
        return this._protoId;
    }

    get examId() {
        return this._examId;
    }

    find(protoId: string, id: string): Promise<ReadResult<Examination>> {
        this._called = true;
        this._protoId = protoId;
        this._examId = id;

        if (this.result !== undefined) {
            return Promise.resolve(read(this.result));
        } else {
            return Promise.reject(error(ErrorType.InternalError));
        }
    }

    reset() {
        this._called = false;
        this.result = undefined;
    }

    all(): never { throw new Error("Not implemented"); }
    add(): never { throw new Error("Not implemented"); }
    update(): never { throw new Error("Not implemented"); }
    setParameter(): never { throw new Error("Not implemented"); }
    rename(): never { throw new Error("Not implemented"); }
}

export class StubSignalsClient implements ISignalsClient {

    private _readCalledCount = 0;
    private _readRequests: SignalDescription[] = [];

    public signalData?: (signal: SignalReadRequest) => any;

    get readCount() {
        return this._readCalledCount;
    }

    get readRequests() {
        return this._readRequests;
    }

    download(signal: SignalReadRequest): Promise<ReadResult<undefined>> {
        const {output, ...rest} = signal;
        this._readCalledCount++;
        this._readRequests.push(rest);

        if (this.signalData !== undefined) {
            signal.output.end(this.signalData(signal));
            return Promise.resolve(read(undefined));
        } else {
            return Promise.resolve(error(ErrorType.InternalError));
        }
    }

    reset() {
        this._readCalledCount = 0;
        this._readRequests = [];
        this.signalData = undefined;
    }

    upload(): never { throw new Error("Not implemented"); }
    list(): never { throw new Error("Not implemented"); }
    rename(): never { throw new Error("Not implemented"); }
}

export class StubSignalParser implements ISignalParser {

    public onValidate: (buffer: Buffer) => boolean;
    public onParse: (data: Buffer) => SignalData;

    validate(data: Buffer): boolean {
        return this.onValidate(data);
    }

    parse(data: Buffer): SignalData {
        return this.onParse(data);
    }

    public rejectAll() {
        this.onValidate = () => false;
        this.onParse = () => <any>undefined;
    }

    public acceptAll() {
        this.onValidate = () => true;
        this.onParse = () => realSignal.data;
    }

}

export class StubParamsExaminationsClient implements IExaminationsClient {

    private _called: boolean = false;
    private _protoId: string;
    private _examId: string;
    private _paramData: [string, Object][] = [];

    public success = false;

    get called() {
        return this._called;
    }

    get protoId() {
        return this._protoId;
    }

    get examId() {
        return this._examId;
    }

    get paramData() {
        return this._paramData;
    }

    setParameter(protoId: string, id: string, paramName: string, value: Object): Promise<UpdateResult> {
        this._called = true;
        this._protoId = protoId;
        this._examId = id;
        this._paramData.push([paramName, value]);

        if (this.success) {
            return Promise.resolve<ObjectUpdated>({ type: "ObjectUpdated", id: "" });
        } else {
            return Promise.resolve(error(ErrorType.InternalError));
        }
    }

    reset() {
        this._called = false;
        this._paramData = [];
        this.success = false;
    }

    find(): never { throw new Error("Not implemented"); }
    all(): never { throw new Error("Not implemented"); }
    add(): never { throw new Error("Not implemented"); }
    update(): never { throw new Error("Not implemented"); }
    rename(): never { throw new Error("Not implemented"); }
}

export class StubWorker implements IWorker {

    private _called: boolean = false;
    private _protoId: string;
    private _examId: string;
    private _paramName: string;

    public delay = 0;
    public result?: (protoId: string, examId: string, paramName: string) => Object;

    get called() {
        return this._called;
    }

    get protoId() {
        return this._protoId;
    }

    get examId() {
        return this._examId;
    }

    get paramName() {
        return this._paramName;
    }

    compute(protoId: string, examId: string, paramName: string): Promise<Errored | Computed> {
        this._called = true;
        this._protoId = protoId;
        this._examId = examId;
        this._paramName = paramName;

        return new Promise<Errored | Computed>(resolve => {
            setTimeout(() => {
                if (this.result !== undefined) {
                    resolve({ type: "Computed", data: this.result(protoId, examId, paramName) });
                } else {
                    resolve({ type: "Errored", message: "" });
                }
            }, this.delay);
        });

    }

    reset() {
        this._called = false;
        this.delay = 0;
        this.result = undefined;
    }

}

export class StubProtocolsClient implements IProtocolsClient {

    public result?: Protocol;

    find(id: string): Promise<ReadResult<Protocol>> {
        if (this.result === undefined) {
            return Promise.resolve(error(ErrorType.InternalError));
        } else {
            return Promise.resolve(read(this.result));
        }
    }

    reset() {
        this.result = undefined;
    }

    all(): never {
        throw new Error("Not implemented");
    }
    add(): never {
        throw new Error("Not implemented");
    }
    update(): never {
        throw new Error("Not implemented");
    }
}
