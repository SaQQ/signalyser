import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);
export const should = chai.should();

// Override default options for server winston, not
// tslint:disable-next-line:no-require-imports
const winston = require("../server/node_modules/winston/lib/winston");
winston.configure({ transports: [] });
