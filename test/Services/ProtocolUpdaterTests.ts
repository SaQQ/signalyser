import { should } from "../Startup";

import { ProtocolUpdate, Protocol } from "../../server/Contract";
import { ProtocolUpdater } from "../../server/Services/ProtocolUpdater";

import * as stub from "./StubData";

describe("Services/ProtocolUpdater", function () {
    const protocols = new stub.StubProtocolsClient();
    const examinations = new stub.StubExaminationsClient();
    const signals = new stub.StubSignalsClient();

    const updater = new ProtocolUpdater(protocols, examinations, signals);

    beforeEach(function () {
        protocols.reset();
        examinations.reset();
        signals.reset();
    });

    it("returns success on valid update", function () {
        return updater.update(UpdateCmd.id, UpdateCmd).should.eventually
            .deep.equal({ type: "ObjectUpdated", id: UpdateCmd.id });
    });

    it("updates db protocol", function () {
        return updater.update(UpdateCmd.id, UpdateCmd).should.eventually.be.fulfilled
            .then(() => {
                should.exist(protocols.id);
                should.exist(protocols.proto);

                protocols.id.should.equal(UpdateCmd.id);
                protocols.proto.should.deep.equal({
                    id: UpdateCmd.id,
                    name: UpdateCmd.name,
                    stages: [{ name: UpdateCmd.stages[0].newName }],
                    signals: [],
                    tags: [{ name: UpdateCmd.tags[0].newName }],
                    params: [{
                        name: UpdateCmd.params[0].newName,
                        formula: UpdateCmd.params[0].formula,
                        compiledFormula: UpdateCmd.params[0].compiledFormula
                    }],
                });
            });
    });

    it("fails when protocol update fails", function () {
        protocols.updateError = true;
        return updater.update(UpdateCmd.id, UpdateCmd).should.eventually
            .deep.equal({ type: "DbActionError", error: 1 });
    });

    it("renames tags", function () {
        const rename = { name: "old-name", newName: "new-name" };
        return updater.update("id", {
            id: "id", name: "name", stages: [], signals: [], params: [],
            tags: [rename]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(examinations.protoId);
            should.exist(examinations.tags);

            const result = { oldName: rename.name, newName: rename.newName };
            examinations.protoId.should.equal("id");
            examinations.tags.should.have.deep.members([result]);
        });
    });

    it("renames params", function () {
        const rename = { name: "old-name", newName: "new-name", formula: "", newFormula: "", newCompiledFormula: "", compiledFormula: "" };
        return updater.update("id", {
            id: "id", name: "name", stages: [], signals: [], tags: [],
            params: [rename]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(examinations.protoId);
            should.exist(examinations.params);

            const result = { oldName: rename.name, newName: rename.newName };
            examinations.protoId.should.equal("id");
            examinations.params.should.have.deep.members([result]);
        });
    });

    it("renames signals", function () {
        const rename = { name: "old-name", newName: "new-name" };
        return updater.update("id", {
            id: "id", name: "name", stages: [], tags: [], params: [],
            signals: [rename]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(signals.protoId);
            should.exist(signals.signals);

            const result = { oldName: rename.name, newName: rename.newName };
            signals.protoId.should.equal("id");
            signals.signals.should.have.deep.members([result]);
        });
    });

    it("renames stages", function () {
        const rename = { name: "old-name", newName: "new-name" };
        return updater.update("id", {
            id: "id", name: "name", signals: [], tags: [], params: [],
            stages: [rename]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(signals.protoId);
            should.exist(signals.stages);

            const result = { oldName: rename.name, newName: rename.newName };
            signals.protoId.should.equal("id");
            signals.stages.should.have.deep.members([result]);
        });
    });

    it("fails silently when examinations cannot be updated", function () {
        examinations.updateError = true;
        return updater.update(UpdateCmd.id, UpdateCmd).should.eventually
            .deep.equal({ type: "ObjectUpdated", id: UpdateCmd.id });
    });

    it("fails silently when signals cannot be updated", function () {
        signals.updateError = true;
        return updater.update(UpdateCmd.id, UpdateCmd).should.eventually
            .deep.equal({ type: "ObjectUpdated", id: UpdateCmd.id });
    });

    it("does not try to update exam/signals if protocol update fails", function () {
        protocols.updateError = true;
        return updater.update(UpdateCmd.id, UpdateCmd).should.eventually.be.fulfilled
            .then(() => {
                should.not.exist(examinations.protoId);
                should.not.exist(signals.protoId);
            });
    });

    it("does not rename tag if name hasn't changed", function () {
        return updater.update("id", {
            id: "id", name: "name", signals: [], params: [], stages: [],
            tags: [{ name: "name", newName: "name" }]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(examinations.protoId);
            should.exist(examinations.tags);

            examinations.protoId.should.equal("id");
            examinations.tags.should.be.empty;
        });
    });

    it("does not rename param if name hasn't changed", function () {
        return updater.update("id", {
            id: "id", name: "name", signals: [], tags: [], stages: [],
            params: [{ name: "name", newName: "name", formula: "", compiledFormula: "" }]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(examinations.protoId);
            should.exist(examinations.params);

            signals.protoId.should.equal("id");
            signals.stages.should.be.empty;
        });
    });

    it("does not rename signal if name hasn't changed", function () {
        return updater.update("id", {
            id: "id", name: "name", tags: [], params: [], stages: [],
            signals: [{ name: "name", newName: "name" }]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(signals.protoId);
            should.exist(signals.signals);

            signals.protoId.should.equal("id");
            signals.signals.should.be.empty;
        });
    });

    it("does not rename signal if name hasn't changed", function () {
        return updater.update("id", {
            id: "id", name: "name", tags: [], params: [], signals: [],
            stages: [{ name: "name", newName: "name" }]
        }).should.eventually.be.fulfilled.then(() => {
            should.exist(signals.protoId);
            should.exist(signals.stages);

            signals.protoId.should.equal("id");
            signals.stages.should.be.empty;
        });
    });

    const UpdateCmd: ProtocolUpdate = {
        id: "id",
        name: "new-name",
        stages: [{ name: "", newName: "stage" }],
        signals: [],
        tags: [{ name: "old-tag", newName: "new-tag" }],
        params: [{
            name: "param",
            newName: "param",
            formula: "formula",
            compiledFormula: "compiled formula"
        }]
    };
});
