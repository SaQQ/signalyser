'use strict';

const fs = require('fs');
const del = require('del');
const electron = require('electron');
const connect = require('electron-connect');
const childProcess = require('child_process');
const builder = require('electron-builder');

const gulp = require('gulp');
const typescript = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const merge = require('merge-stream');
const sass = require('gulp-sass');
const peg = require('gulp-peg');
const watch = require('gulp-watch');
const tslint = require('gulp-tslint');
const transform = require('gulp-transform');
const sequence = require('run-sequence');
const envify = require('loose-envify/replace');

const { copyOptional } = require('./gulp-scripts.js');

const paths = {

    tsconfig: './src/tsconfig.json',
    typescriptWatch: ['./src/**/*.ts', './src/**/*.tsx', '!./src/node_modules/**', '!./src/**/*.d.ts', '!./src/main.ts'],
    typescript: ['./src/**/*.ts', './src/**/*.tsx', '!./src/node_modules/**', '!./src/**/*.d.ts'],

    sass: './styles/**/*.scss',
    prebuiltCss: './styles/**/*.css',

    prebuiltHtml: ['./src/**/*.html', '!./src/node_modules/**'],

    peg: ['src/**/*.peg'],

    contracts: ['./server/Contract.ts', './server/ExaminationContract.ts'],
    stringified: ['./server/ExaminationContract.ts'],

    nodeModules: './src/node_modules/**',
    fonts: './styles/fonts/**',

    additionalFiles: [
        './src/package.json'
    ],

    stringifyDist: './src/StringifiedModules',
    autogenDist: './src',
    dist: './dist/app',
    stylesDist: './dist/app/styles',
    nodeDist: './dist/app/node_modules',
    fontsDist: './dist/app/styles/fonts',

    packTemp: './dist/app-pack-tmp',
    packedApps: './dist/packed-app'
};

const AutoGenComment = "// THIS FILE IS GENERATED AUTOMATICALLY, DO NOT MODIFY\n\n\n";

const tsProject = typescript.createProject(paths.tsconfig);

gulp.task('clean:app', () => del(paths.dist));
gulp.task('clean:app:all', () => del([
    paths.dist,
    paths.nodeModules
]));

gulp.task('peg:app', () => {
    let dist = gulp.src(paths.peg)
        .pipe(sourcemaps.init())
        .pipe(peg().on('error', console.log))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            destPath: paths.dist,
            sourceRoot: '../src'
        }))
        .pipe(gulp.dest(paths.dist));
    let src = gulp.src(paths.peg)
        .pipe(peg().on('error', console.log))
        .pipe(gulp.dest(paths.autogenDist));
    return merge(dist, src);
});

gulp.task('autogen:app', () => {
    let stringify = gulp.src(paths.stringified)
        .pipe(transform('utf8', content => AutoGenComment + "export default \"" + content.toString().split("\"").join("\\\"").split("\n").join("\\\n") + "\";\n"))
        .pipe(gulp.dest(paths.stringifyDist))

    let copy = gulp.src(paths.contracts)
        .pipe(transform('utf8', c => AutoGenComment + c))
        .pipe(gulp.dest(paths.autogenDist));

    return merge(stringify, copy);
});

gulp.task('tslint:app', ['peg:app'], () => {
    return gulp.src(paths.typescript)
        .pipe(tslint())
        .pipe(tslint.report());
});

gulp.task('typescript:app', ['tslint:app'], () =>
    gulp.src(paths.typescript)
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .js.pipe(sourcemaps.write('.', {
            includeContent: false,
            destPath: paths.dist,
            sourceRoot: '../src'
        }))
        .pipe(gulp.dest(paths.dist))
);

gulp.task('sass:app', () =>
    gulp.src(paths.sass)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.stylesDist))
);

gulp.task('copy-static:app:front', () =>
    merge(
        gulp.src(paths.prebuiltHtml).pipe(gulp.dest(paths.dist)),
        gulp.src(paths.prebuiltCss).pipe(gulp.dest(paths.stylesDist)),
        gulp.src(paths.fonts).pipe(gulp.dest(paths.fontsDist)),
        gulp.src(paths.additionalFiles).pipe(gulp.dest(paths.dist))
    )
);

gulp.task('copy-static:app:node_modules', () =>
    copyOptional(paths.nodeModules, paths.nodeDist)
);

gulp.task('copy-static:app', [
    'copy-static:app:front',
    'copy-static:app:node_modules'
]);

gulp.task('build:app', ['autogen:app', 'typescript:app', 'sass:app', 'copy-static:app']);

function spawnApp(closeCallback) {
    return childProcess.spawn(electron, ['.', '--remote-debugging-port=9222'], {
        stdio: 'inherit',
        cwd: paths.dist
    }).on('close', closeCallback);
}

function watchApp(closeCallback) {
    const electronApp = connect.server.create({
        electron: electron,
        path: paths.dist,
        stopOnClose: true,
        spawnOpt: {
            stdio: 'inherit',
            env: Object.assign(process.env, { "LIVERELOAD": "1" })
        }
    });
    const exitWhenStopped = (state) => {
        if (state == 'stopped') {
            closeCallback();
        }
    };
    const reload = () => electronApp.reload(exitWhenStopped);

    electronApp.start(['--remote-debugging-port=9222'], exitWhenStopped);

    watch(paths.typescriptWatch, () => gulp.start('typescript:app', reload));
    watch(paths.sass, () => gulp.start('sass:app', reload));
    watch(paths.prebuiltCss, () => gulp.start('copy-static:app:front', reload))
    watch(paths.prebuiltHtml, () => gulp.start('copy-static:app:front', reload));

    return () => electronApp.stop();
}

gulp.task('start:app', ['build:app'], () => spawnApp(() => process.exit()));
gulp.task('watch:app', ['build:app'], () => watchApp(() => process.exit()));

gulp.task('pack:app:pre', cb => sequence('clean:app', 'build:app', cb));
gulp.task('pack:app:del', ['pack:app:pre'], () => del([paths.packTemp, paths.packedApps]));
gulp.task('pack:app:copy', ['pack:app:del'], () =>
    gulp.src([`${paths.dist}/**`, `!${paths.dist}/**/*.map`])
        .pipe(gulp.dest(paths.packTemp))
);
gulp.task('pack:app:cleanup', ['pack:app:copy'], cb => {
    childProcess.exec('npm prune --production', { cwd: paths.packTemp }, err => {
        if (err) {
            return cb(err);
        } else {
            cb();
        }
    });
});
gulp.task('pack:app:envify', ['pack:app:cleanup'], () =>
    gulp.src([`${paths.packTemp}/**/*.js`, `!${paths.packTemp}/node_modules/**`])
        .pipe(transform('utf8', cnt => envify(cnt.toString(), [{ NODE_ENV: 'production' }])))
        .pipe(gulp.dest(paths.packTemp))
);
gulp.task('pack:app:do', ['pack:app:envify'], cb => {
    let win = builder.Platform.WINDOWS.createTarget("zip", builder.Arch.ia32, builder.Arch.x64);
    let lin = builder.Platform.LINUX.createTarget("tar.xz", builder.Arch.ia32, builder.Arch.x64);
    builder.build({
        targets: new Map([...win, ...lin]),
        config: {
            asar: true,
            npmRebuild: false,
            directories: {
                app: paths.packTemp,
                output: paths.packedApps,
            }
        }
    }).then(() => cb()).catch(cb);
});
gulp.task('pack:app', ['pack:app:do'], () => del(paths.packDist));

module.exports = { spawnApp, watchApp };
