/// <reference path="../node_modules/@types/redux-thunk/index.d.ts" />

import * as Redux from "redux";
import { LOCATION_CHANGE, RouterAction, goBack } from "react-router-redux";

/**
 * Action meant to be handled by a promise middleware. FSA-compliant.
 *
 * @template T The result of the promise.
 */
export interface PromiseAction<T> extends Redux.Action {
    payload: PromiseLike<T> | { promise: PromiseLike<T> };
}

/**
 * Action meant to be handled by a promise middleware. FSA-compliant.
 *
 * @template T The result of the promise.
 * @template TData The data that may be passed to the PendingPromiseAction.
 */
export interface PromiseDataAction<T, TData> extends Redux.Action {
    payload: { promise: PromiseLike<T>, data: TData };
}

/**
 * Action that is dispatched right after PromiseAction is gets dispatched.
 * FSA-compliant.
 *
 * @template T The result of the promise.
 */
export interface PendingPromiseAction<T> extends Redux.Action {
}

/**
 * Action that is dispatched right after PromiseAction is gets dispatched.
 * FSA-compliant.
 *
 * @template T The result of the promise.
 * @template TData The type of additional data passed to PromiseActionData.
 */
export interface PendingPromiseDataAction<T, TData> extends Redux.Action {
    payload: TData;
}

/**
 * Action that is dispatched by the promise middleware when the promise gets
 * rejected. FSA-compliant.
 */
export interface RejectedPromiseAction<T> extends Redux.Action {
    error: true;
    payload: any;
}

/**
 * Action that is dispatched by the promise middleware when the promise gets
 * fullfilled. FSA-compliant.
 */
export interface FulfilledPromiseAction<T> extends Redux.Action {
    payload: T;
}

/**
 * Action type that the reducer should take.
 *
 * @template A Real action that the reducer handles (may be union type).
 */
export type Action<A extends Redux.Action> = A | Redux.Action;

/**
 * Type checker for action reducers. Don't use directly as the type guard won't
 * be recognized.
 *
 * @template A Real action that the checker should guard for.
 */
export type ActionCheck<A extends Redux.Action> = (a: Action<A>) => a is A;

/**
 * Strongly-typed reducer.
 *
 * @template S The state.
 * @template A Real action that the reducer handles (may be union type).
 */
export type Reducer<S, A extends Redux.Action> = (state: S, action: Action<A>) => S;

/**
 * Strongly-typed reducer.
 *
 * @template S The state.
 * @template A Real action that the reducer handles (may be union type).
 */
export type ProperReducer<S, A extends Redux.Action> = (state: S, action: A) => S;

/**
 * Creates a reducer that handles only a single action on a subtree of the state.
 *
 * @template S The state that is handled by the
 */
export function createReducer<S, A extends Redux.Action>(
    def: S, check: ActionCheck<A>, perform: ProperReducer<S, A>): Reducer<S, A> {
    return (s = def, a) => {
        if (check(a)) {
            return perform(s, a);
        }
        return s;
    };
}

/**
 * Check whether the action is a router-related action.
 */
export function isRouterAction(action: Redux.Action): action is RouterAction {
    return action.type === LOCATION_CHANGE;
}

/**
 * Check whether the action is a page load action.
 */
export function pageGotLoaded(action: Redux.Action, path: string): boolean {
    return isRouterAction(action) && (
        (typeof action.payload === "string" && action.payload === path) ||
        (typeof action.payload === "object" && action.payload.pathname === path)
    );
}

/**
 * Creates id reducer (does nothing) with the default value. Ideal for
 * combineReducers and no-reducer clauses.
 *
 * Beware performance implications!
 */
export function idReducer<T>(def: T) {
    return (s: T = def, _: Redux.Action) => s;
}

/**
 * Thunk action without return value (to prevent from invalid use with
 * promise middleware) and without extra arguments (not used here).
 */
export type ThunkAction<S> = Redux.ThunkAction<void, S, void>;

/**
 * Combines reducer by applying them in the order they are specified.
 */
export function combineReducersInOrder<S>(...reducers: Redux.Reducer<S>[]) {
    return (state: S, action: Redux.Action) =>
        reducers.reduce((v, f) => f(v, action), state);
}

/**
 * Navigates back when the promise gets rejected.
 */
export function navigateBackOnError<S>(dispatch: Redux.Dispatch<S>) {
    return (err: any) => {
        dispatch(goBack());
        return Promise.reject(err);
    };
}

export { Action as ReduxAction, Dispatch, combineReducers } from "redux";
export { connect, Provider } from "react-redux";

declare module "redux" {
    export interface Dispatch<S> {
        <A extends PromiseAction<T>, T>(action: A): Promise<T>;
    }
}
