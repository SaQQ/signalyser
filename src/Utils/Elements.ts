import * as immutable from "immutable";
import { v1 as uuid } from "uuid";

const UserIdRegex = /^[a-zA-Z0-9_]+$/;

export interface Named {
    name: string;
}

export interface NamedWithId extends Named {
    id: string;
}

export function namedElementExists<T extends Named>(value: string, names: immutable.Iterable.Indexed<T>) {
    return names.some(v => v.name === value);
}

export function isValidUserId(value: string) {
    return value !== undefined && value !== null && UserIdRegex.test(value);
}

export function generateUniqueName<T extends Named>(prefix: string, names: immutable.Iterable.Indexed<T>) {
    if (!namedElementExists(prefix, names)) {
        return prefix;
    }
    for (let i = 1; ; i++) {
        let name = prefix + i;
        if (!namedElementExists(name, names)) {
            return name;
        }
    }
}

/**
 * For specified collection of named elements returns error message
 * depending whether selected element's name is valid and unique in collection
 *
 * @param element - Element to validate.
 * @param allElements - Collection of elements to validate against.
 */
export function validateNamesCollection<T extends NamedWithId>(element: T, allElements: immutable.List<T>): string {
    if (element.name.length === 0) {
        return "Empty names are not allowed";
    } else if (!isValidUserId(element.name)) {
        return "The name must consist of only a-z, A-Z, 0-9 and _ chars";
    } else if (!allElements.every(e => e.id === element.id || e.name !== element.name)) {
        return "Element of the same name already exists, change one of them";
    } else {
        return "";
    }
}

export function selectUniqueNamed<TInput extends Named>(elements: TInput[]) {
    let result: TInput[] = [];
    elements.forEach(e => {
        if (result.findIndex(r => r.name === e.name) === -1) {
            result.push(e);
        }
    });
    return result;
}
