import * as React from "react";
import ErrorOutline from "material-ui/svg-icons/alert/error-outline";
import Subheader from "material-ui/Subheader";

import {grey600} from "material-ui/styles/colors";

import * as consts from "../Consts";

export interface NotFoundProps {
    message: string;
}

const style = {
    width: `${consts.NotFoundIconSize}px`,
    height: `${consts.NotFoundIconSize}px`,
    paddingRight: `${consts.NotFoundIconSize / 2}px`
};

export const NotFound = (props: NotFoundProps) => <div className="loading-screen">
    <ErrorOutline style={style} color={grey600} />
    <span style={{ color: grey600, fontSize: consts.NotFoundFontSize }}>{props.message}</span>
</div>;
