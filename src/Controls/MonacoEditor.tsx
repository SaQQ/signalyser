/// <reference path="../typings/monaco/index.d.ts" />
import * as React from "react";
import * as Monaco from "monaco";

import ContractLib from "../StringifiedModules/ExaminationContract";

declare function amdRequire(moduleNames: string[], onLoad: (...args: any[]) => void): void;
declare let global: { monaco: typeof Monaco };

let monaco: typeof Monaco;

// amdRequire(["vs/editor/editor.main"], () => {
//     monaco = global.monaco;

//     monaco.languages.typescript.typescriptDefaults.addExtraLib(
//         "declare module \"Contract\" {\n" + ContractLib + "}\n"
//     );
// });

export interface MonacoEditorProps {
    value?: string;
    containerStyle?: React.CSSProperties;
    options?: Monaco.editor.IEditorConstructionOptions;
    onDestroyed?: (val: string) => void;
}

export interface MonacoEditorState {

}

export class MonacoEditor extends React.Component<MonacoEditorProps, MonacoEditorState> {
    refs: {
        [key: string]: (Element);
        container: (HTMLDivElement);
    };

    editor: Monaco.editor.IStandaloneCodeEditor;

    constructor(props: MonacoEditorProps) {
        super(props);
    }

    componentDidMount() {
        this.initMonaco();
    }

    componentWillUnmount() {
        this.destroyMonaco();
    }

    componentWillUpdate(nextProps: MonacoEditorProps) {
        this.editor.setValue(nextProps.value || "");
    }

    initMonaco() {
        const { value, options } = this.props;
        const containerElement = this.refs.container;

        this.editor = monaco.editor.create(containerElement, {
            value,
            ...options,
        });
    }

    destroyMonaco() {
        const { onDestroyed } = this.props;

        if (onDestroyed !== undefined) {
            onDestroyed(this.editor.getValue());
        }

        this.editor.dispose();
    }

    render() {
        return <div ref="container" style={this.props.containerStyle} />;
    }
}
