/// <reference path="../node_modules/@types/material-ui/index.d.ts" />

import * as React from "react";
import TextField from "material-ui/TextField";

import * as consts from "../Consts";

interface SuccessResult {
    type: "SuccessResult";
}

interface WarningResult {
    type: "WarningResult";
    message: string;
}

interface ErrorResult {
    type: "ErrorResult";
    message: string;
}

export type ValidationResult = SuccessResult | WarningResult | ErrorResult;

export function success(): ValidationResult {
    return { type: "SuccessResult" };
}

export function warning(message: string): ValidationResult {
    return { type: "WarningResult", message };
}

export function error(message: string): ValidationResult {
    return { type: "ErrorResult", message };
}

export interface ValidatedTextFieldProps extends __MaterialUI.TextFieldProps {
    readonly value: string;
    readonly onValueChange: (newVal: string) => void;
    readonly validate: (value: string) => ValidationResult;

    readonly warningStyle?: React.CSSProperties;
}

interface ValidatedTextFieldState {
    readonly currVal: string;
    readonly validation: ValidationResult;
}

// TypeScript does not allow spread/rest operator on generic types, so we are
// unable to make StatefulTextField generic on props/state types (and we need it
// to add isValid). Therefore - duplicate the logic.
export class ValidatedTextField extends React.Component<ValidatedTextFieldProps, ValidatedTextFieldState> {

    constructor(props: ValidatedTextFieldProps) {
        super(props);

        this.state = { currVal: props.value || "", validation: success() };
    }

    render() {
        let { onChange, onValueChange, onBlur, onKeyDown, validate,
            errorText, errorStyle, warningStyle, value, ...rest } = this.props;
        warningStyle = warningStyle || consts.DefaultWarningStyle;

        let realStyle =
            this.state.validation.type === "ErrorResult" ? errorStyle :
            this.state.validation.type === "WarningResult" ? warningStyle :
            undefined;
        let realMessage =
            this.state.validation.type === "SuccessResult" ? "" :
            this.state.validation.message;

        return <TextField
            {...rest}
            errorText={realMessage}
            errorStyle={realStyle}
            value={this.state.currVal}
            onKeyDown={this.onKeyDown}
            onChange={this.onChange} onBlur={this.onBlur} />;
    }

    private onChange = (e: React.FormEvent<{}>, newValue: string) => {
        if (this.props.onChange !== undefined) {
            this.props.onChange(e, newValue);
        }

        let oldValue = this.state.currVal;
        let result = this.props.validate(newValue);
        if (result.type === "ErrorResult") {
            this.setState({ currVal: oldValue, validation: result });
        } else {
            this.setState({ currVal: newValue, validation: result });
        }
    }

    private onBlur = (e: React.FocusEvent<{}>) => {
        if (this.props.onBlur !== undefined) {
            this.props.onBlur(e);
        }
        this.props.onValueChange(this.state.currVal);
    }

    private onKeyDown = (e: React.KeyboardEvent<{}>) => {
        if (this.props.onKeyDown !== undefined) {
            this.props.onKeyDown(e);
        }
        if (e.keyCode === 13) {
            this.props.onValueChange(this.state.currVal);
        }
    }
}
