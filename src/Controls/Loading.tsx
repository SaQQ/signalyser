import * as React from "react";
import CircularProgress from "material-ui/CircularProgress";

import * as consts from "../Consts";

export const LoadingScreen = () => <div className="loading-screen">
    <CircularProgress mode="indeterminate" size={consts.LoadingScreenSpinnerSize} />
</div>;
