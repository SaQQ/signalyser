import { app, BrowserWindow } from "electron";

let mainWindow: Electron.BrowserWindow | null;

if (process.env.NODE_ENV === "development") {
    require("electron-debug")(); // tslint:disable-line
}

async function installExtensions() {
    if (process.env.NODE_ENV === "development") {
        const extensions = [
            "REACT_DEVELOPER_TOOLS",
            "REDUX_DEVTOOLS"
        ];

        // tslint:disable-next-line
        const installer = require("electron-devtools-installer");
        const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
        for (const name of extensions) {
            try {
                await installer.default(installer[name], forceDownload);
            } catch (e) { }
        }
    }
}

function createMainWindow() {
    mainWindow = new BrowserWindow({
        darkTheme: true,
        title: "Electron tests",
        width: 1024,
        height: 768,
        acceptFirstMouse: true,
        titleBarStyle: "hidden"
    });
    mainWindow.setMenu(<any>null);

    mainWindow.loadURL(`file://${__dirname}/index.html`);

    mainWindow.on("close", () => {
        mainWindow = null;
    });

    if (process.env.NODE_ENV === "development" && !!process.env.LIVERELOAD) {
        // tslint:disable-next-line:no-require-imports
        const {client} = require("electron-connect");
        client.create(mainWindow);
    }
}

app.on("ready", async () => {
    await installExtensions();

    createMainWindow();
});

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (mainWindow == null) {
        createMainWindow();
    }
});
