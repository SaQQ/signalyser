import { combineReducers, combineReducersInOrder } from "./Utils";
import { routerReducer } from "react-router-redux";

import { protocolReducer } from "./Protocols/Reducer";
import { examinationsReducer } from "./Examinations/Reducers";
import { settingsReducer } from "./Settings/Reducer";

export const appStateReducers = combineReducers({
    ["protocols"]: protocolReducer,
    ["examinations"]: examinationsReducer,
    ["settings"]: settingsReducer,
    ["routing"]: routerReducer
});
