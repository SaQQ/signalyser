export interface SettingsState {
    readonly isOpen: boolean;
    readonly serverAddress: string;
}
