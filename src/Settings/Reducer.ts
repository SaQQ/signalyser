import { Action } from "../Utils/Redux";

import {
    isOpenSettings, isSaveSettings, isDiscardSettings, SettingsAction
} from "./Actions";

import { SettingsState } from "./State";
import { Settings } from "./Settings";

export function settingsReducer(state: SettingsState, action: Action<SettingsAction>): SettingsState {
    if (isOpenSettings(action)) {
        return { isOpen: true, serverAddress: state.serverAddress };
    } else if (isSaveSettings(action)) {
        return { isOpen: false, serverAddress: action.serverAddress };
    } else if (isDiscardSettings(action)) {
        return { isOpen: false, serverAddress: Settings.serverAddress };
    } else {
        return state || {
            isOpen: false,
            serverAddress: Settings.serverAddress
        };
    }
}
