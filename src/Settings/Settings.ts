const SettingsKey = "settings";
const DefaultServerAddress = "http://localhost:5000";

export interface StoredSettings {
    serverAddress: string;
}

function tryParse(str: string | null): any {
    if (str === null) {
        return {};
    }

    try {
        return JSON.parse(str);
    } catch (e) {
        return {};
    }
}

class SettingsKeeper {

    private _settings: StoredSettings;

    constructor() {
        let str = localStorage.getItem(SettingsKey);
        this._settings = this.coerce(tryParse(str));
    }

    public get settings() {
        return this._settings;
    }

    public get serverAddress() {
        return this._settings.serverAddress;
    }

    update(settings: StoredSettings) {
        this._settings = settings;

        localStorage.setItem(SettingsKey, JSON.stringify(settings));
    }

    private coerce(saved: StoredSettings): StoredSettings {
        return {
            serverAddress: saved.serverAddress || DefaultServerAddress
        };
    }
}

export const Settings = new SettingsKeeper();
