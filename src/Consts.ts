export const DefaultTags = ["Name", "Surname", "PESEL"];

export const DefaultWarningStyle: React.CSSProperties = { color: "orange" };

export const TagBarControlStyle: React.CSSProperties = { color: "white" };
export const TagBarButtonStyle: React.CSSProperties = { color: "white", margin: "10px 5px" };
export const TagBarTextFieldStyle: React.CSSProperties = { color: "white", width: "400px" };
export const LoadingScreenSpinnerSize = 80;
export const NotFoundIconSize = 64;
export const NotFoundFontSize = 24;

export const FormulaEditorHeaderSize = 18;
export const FormulaEditorWidth = 800;
export const FormulaEditorHeight = 600;
