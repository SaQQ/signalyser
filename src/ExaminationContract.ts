// THIS FILE IS GENERATED AUTOMATICALLY, DO NOT MODIFY


export const SignalMimeType = "application/vnd.examination-signal";

/**
 *  Dictionary name -> object, used in parameters and tags
 */
export interface AttributesCollection<T> {
    [name: string]: T;
}

/**
 * Type of tag value - either number or string
 */
export type TagValue = number | string;

/**
 * Type of post signal value
 */

/**
 * Examinatio ID and tags for purpose of uploading to server
 */
export interface ExaminationWithTags {
    /**
     * Unique signal ID
     */
    id: string;
    /**
     * Tags that have been set
     */
    tags: AttributesCollection<TagValue>;
}

/**
 * Basic examination data - id, tags, and parameters
 */
export interface ExaminationDescription extends ExaminationWithTags {
    /**
     * Caluclated parameters
     */
    params: AttributesCollection<Object>;
}

/**
 * Signal placement in the protocol
 */
export interface AvailableSignal {
    /**
     * Stage name
     */
    stage: string;
    /**
     * Signal name
     */
    signal: string;
}

/**
 * Raw signal data - possibly mulitchannel
 */
export interface SignalData {
    /**
     * Time values of samples
     */
    timeData: ArrayLike<number>;
    /**
     * Values of all channels
     */
    seriesData: ArrayLike<number>[];
}

/**
 * Signal data placed in protocol
 */
export interface FullSignal extends AvailableSignal {
    data: SignalData;
}

/**
 * Examination with tags, parameters and list of avalaible signals
 */
export interface Examination extends ExaminationDescription {
    signals: AvailableSignal[];
}

/**
 * Complete examination with signal data
 */
export interface FullExamination extends ExaminationDescription {
    signals: FullSignal[];
}

/**
 * Basic examination data - id, tags, and parameters
 */
