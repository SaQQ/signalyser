import * as injectTapEvent from "react-tap-event-plugin";

let isArray: (arg: any) => arg is Array<any>;

function isArrayOrIsFloat32Array(arg: any): arg is Array<any> {
    return isArray(arg) || arg instanceof Float32Array;
}

function supportTypedArraysInPlotly() {
    isArray = Array.isArray;
    Array.isArray = isArrayOrIsFloat32Array;
}

function restoreIsArray() {
    Array.isArray = isArray;
}

export function applyHacks() {
    supportTypedArraysInPlotly();
    injectTapEvent();
}

export function revertHacks() {
    restoreIsArray();
}
