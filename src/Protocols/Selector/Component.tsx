import * as React from "react";
import * as immutable from "immutable";

import IconButton from "material-ui/IconButton";
import FlatButton from "material-ui/FlatButton";
import Subheader from "material-ui/Subheader";
import { List, ListItem } from "material-ui/List";
import { Toolbar, ToolbarGroup } from "material-ui/Toolbar";

import NavigationRefresh from "material-ui/svg-icons/navigation/refresh";
import ModeEdit from "material-ui/svg-icons/editor/mode-edit";

import * as consts from "../../Consts";

import { Header, LoadingScreen, NotFound } from "../../Controls";
import { Dispatch, connect } from "../../Utils";

import { AppState } from "../../State";
import { ProtocolDescription } from "../../Contract";

import { loadProtocols } from "./Actions";
import { editProtocol, openNewProtocol } from "../Navigation";
import { openExaminationsList } from "../../Examinations/Navigation";

export interface ProtocolSelectorProps {
    isLoading: boolean;
    protocols: immutable.List<ProtocolDescription>;
}

export interface ProtocolSelectorActions {
    refreshData: () => void;
    openProtocol: (id: string) => void;
    newProtocol: () => void;
    openExaminationsList: (protocolId: string) => void;
}

class ProtocolSelectorControl extends React.Component<ProtocolSelectorProps & ProtocolSelectorActions, {}> {

    componentWillMount() {
        this.props.refreshData();
    }

    render() {
        return <div>
            <Header title="Open protocol">
                <Toolbar>
                    <ToolbarGroup lastChild={true}>
                        <FlatButton onTouchTap={this.props.newProtocol} style={consts.TagBarButtonStyle} label="New protocol" />
                        <IconButton onTouchTap={this.props.refreshData} tooltip="Refresh"><NavigationRefresh {...consts.TagBarControlStyle} /></IconButton>
                    </ToolbarGroup>
                </Toolbar>
            </Header>
            {this.props.isLoading ? <LoadingScreen /> :
                this.props.protocols.size > 0 ?
                <div>
                    <List>
                        <Subheader>Protocols</Subheader>
                        {this.props.protocols.map(p =>
                            <ListItem
                                key={p.id}
                                primaryText={p.name}
                                rightIconButton={
                                    <IconButton
                                        onTouchTap={() => this.props.openProtocol(p.id)}
                                        tooltip="edit" tooltipPosition="bottom-left">
                                        <ModeEdit />
                                    </IconButton>}
                                onTouchTap={() => this.props.openExaminationsList(p.id)} />
                        )}
                    </List>
                </div> : <NotFound message="No protocols were found"/>
            }
        </div>;
    }

}

const mapProps = (state: AppState): ProtocolSelectorProps => ({
    isLoading: state.protocols.list.isLoading,
    protocols: state.protocols.list.loadedProtocols
});

const mapActions = (dispatch: Dispatch<AppState>): ProtocolSelectorActions => ({
    refreshData: () => dispatch(loadProtocols()),
    openProtocol: id => dispatch(editProtocol(id)),
    newProtocol: () => dispatch(openNewProtocol()),
    openExaminationsList: protocolId => dispatch(openExaminationsList(protocolId))
});

export const ProtocolSelector = connect(mapProps, mapActions)(ProtocolSelectorControl);
