import { combineReducersInOrder, idReducer } from "../../../Utils";

import { ProtocolsListState, emptyProtocolsListState } from "../State";
import { loadProtocolsReducer } from "./LoadProtocolsReducer";

const initialState = idReducer(emptyProtocolsListState);

export const protocolsListReducer = combineReducersInOrder(
    initialState,
    loadProtocolsReducer
);
