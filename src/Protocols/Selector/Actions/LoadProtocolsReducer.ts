import { Action } from "../../../Utils";
import { ProtocolsList, ProtocolsListState } from "../State";
import {
    LoadProtocolsResult,
    isLoadProtocolsPending, isLoadProtocolsFulfilled, isLoadProtocolsRejected
} from "./LoadProtocols";

export function loadProtocolsReducer(state: ProtocolsListState, action: Action<LoadProtocolsResult>): ProtocolsListState {
    if (isLoadProtocolsPending(action)) {
        return { isLoading: true, loadedProtocols: ProtocolsList() };
    } else if (isLoadProtocolsFulfilled(action)) {
        return { isLoading: false, loadedProtocols: ProtocolsList(action.payload) };
    } else if (isLoadProtocolsRejected(action)) {
        return { isLoading: false, loadedProtocols: ProtocolsList() };
    }
    return state;
}
