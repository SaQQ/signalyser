import {
    Action, ThunkAction,
    PromiseAction, FulfilledPromiseAction, RejectedPromiseAction
} from "../../../Utils";
import { ServerClient, ProtocolsList, handleClientError } from "../../../Client";

import { AppState } from "../../../State";

interface LoadProtocolsAction extends PromiseAction<ProtocolsList> {
    type: "LoadProtocols";
}
interface LoadProtocolsPendingAction extends PromiseAction<ProtocolsList> {
    type: "LoadProtocols_PENDING";
}
interface LoadProtocolsFulfilledAction extends FulfilledPromiseAction<ProtocolsList> {
    type: "LoadProtocols_FULFILLED";
}
interface LoadProtocolsRejectedAction extends RejectedPromiseAction<ProtocolsList> {
    type: "LoadProtocols_REJECTED";
}

export const isLoadProtocols =
    (a: Action<LoadProtocolsAction>): a is LoadProtocolsAction => a.type === "LoadProtocols";
export const isLoadProtocolsPending =
    (a: Action<LoadProtocolsPendingAction>): a is LoadProtocolsPendingAction => a.type === "LoadProtocols_PENDING";
export const isLoadProtocolsFulfilled =
    (a: Action<LoadProtocolsFulfilledAction>): a is LoadProtocolsFulfilledAction => a.type === "LoadProtocols_FULFILLED";
export const isLoadProtocolsRejected =
    (a: Action<LoadProtocolsRejectedAction>): a is LoadProtocolsRejectedAction => a.type === "LoadProtocols_REJECTED";

export type LoadProtocolsResult =
    LoadProtocolsAction
    | LoadProtocolsPendingAction
    | LoadProtocolsFulfilledAction
    | LoadProtocolsRejectedAction;

export function loadProtocols(): ThunkAction<AppState> {
    return dispatch => dispatch({
        type: "LoadProtocols",
        payload: ServerClient.loadProtocolsList()
    }).catch(handleClientError);
}
