import * as immutable from "immutable";

import { ProtocolDescription } from "../../Contract";

export type ProtocolsList = immutable.List<ProtocolDescription>;
export function ProtocolsList(arr?: ReadonlyArray<ProtocolDescription>) {
    if (arr === undefined) {
        return immutable.List<ProtocolDescription>();
    } else {
        return immutable.List<ProtocolDescription>(arr);
    }
}

export interface ProtocolsListState {
    readonly isLoading: boolean;
    readonly loadedProtocols: ProtocolsList;
}

export const emptyProtocolsListState: ProtocolsListState = {
    isLoading: false,
    loadedProtocols: ProtocolsList()
};
