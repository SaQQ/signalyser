import { combineReducers } from "redux";

import { ProtocolsState } from "./State";
import { editingProtocolReducer } from "./Designer/Actions/Reducers";
import { protocolsListReducer } from "./Selector/Actions/Reducers";

export const protocolReducer = combineReducers<ProtocolsState>({
    ["list"]: protocolsListReducer,
    ["editing"]: editingProtocolReducer
});

