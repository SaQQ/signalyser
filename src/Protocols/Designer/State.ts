import * as immutable from "immutable";

import { StageDescriptor, SignalDescriptor, TagDescriptor, ParameterDescriptor, Protocol } from "../../Contract";

export interface EditingSignalState {
    /**
     * Needed for React UI keys.
     */
    readonly id: string;
    readonly oldName: string;
    readonly name: string;
    readonly error: string;
}

export interface EditingStageState {
    /**
     * Needed for React UI keys.
     */
    readonly id: string;
    readonly oldName: string;
    readonly name: string;
    readonly error: string;
}

export interface EditingTagState {
    /**
     * Needed for React UI keys.
     */
    readonly id: string;
    readonly oldName: string;
    readonly name: string;
    readonly error: string;
}

export interface EditingParamState {
    /**
     * Needed for React UI keys.
     */
    readonly id: string;
    readonly oldName: string;
    readonly name: string;
    readonly formula: string;
    readonly compiledFormula: string;
    readonly compileError: boolean;
    readonly error: string;
}

export type SignalsState = immutable.List<EditingSignalState>;
export type StagesState = immutable.List<EditingStageState>;
export type TagsState = immutable.List<EditingTagState>;
export type ParamsState = immutable.List<EditingParamState>;

export interface EditingProtocolState {
    readonly isLoading: boolean;
    readonly isNew: boolean;
    readonly id: string;
    readonly name: string;
    readonly stages: StagesState;
    readonly signals: SignalsState;
    readonly tags: TagsState;
    readonly params: ParamsState;
}

export const emptyEditingProtocolState: EditingProtocolState = {
    isLoading: false,
    isNew: false,
    id: "",
    name: "New Protocol",
    stages: immutable.List<EditingStageState>(),
    signals: immutable.List<EditingSignalState>(),
    tags: immutable.List<EditingTagState>(),
    params: immutable.List<EditingParamState>()
};
