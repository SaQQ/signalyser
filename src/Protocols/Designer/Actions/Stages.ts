import { Action, ReduxAction } from "../../../Utils";

interface AddNewStageAction extends ReduxAction {
    type: "AddNewStage";
}

interface RenameStageAction extends ReduxAction {
    type: "RenameStage";
    readonly id: string;
    readonly newName: string;
}

interface RemoveStageAction extends ReduxAction {
    type: "RemoveStage";
    readonly id: string;
}

export type StageActions = AddNewStageAction | RenameStageAction | RemoveStageAction;

export const isAddNewStage = (a: Action<AddNewStageAction>): a is AddNewStageAction => a.type === "AddNewStage";
export const isRenameStage = (a: Action<RenameStageAction>): a is RenameStageAction => a.type === "RenameStage";
export const isRemoveStage = (a: Action<RemoveStageAction>): a is RemoveStageAction => a.type === "RemoveStage";

export const addNewStage = (): AddNewStageAction => ({ type: "AddNewStage" });
export const renameStage = (id: string, newName: string): RenameStageAction =>
    ({ type: "RenameStage", id, newName });
export const removeStage = (id: string): RemoveStageAction => ({ type: "RemoveStage", id });
