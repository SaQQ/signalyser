import {
    combineReducers, combineReducersInOrder, idReducer
} from "../../../Utils";

import { EditingProtocolState, emptyEditingProtocolState } from "../State";

import { changeNameReducer, newProtocolReducer } from "./BasicReducers";
import { stagesReducer } from "./StagesReducers";
import { signalsReducer } from "./SignalsReducers";
import { tagsReducer } from "./TagsReducers";
import { paramsReducer } from "./ParamsReducers";

import { loadProtocolReducer } from "./LoadProtocolReducers";
import { saveProtocolReducer } from "./SaveProtocolReducers";

const initialState = idReducer(emptyEditingProtocolState);

const editActionsReducer = combineReducers<EditingProtocolState>({
    ["isLoading"]: idReducer(false),
    ["isNew"]: idReducer(false),
    ["id"]: idReducer(""),
    ["name"]: changeNameReducer,
    ["stages"]: stagesReducer,
    ["signals"]: signalsReducer,
    ["tags"]: tagsReducer,
    ["params"]: paramsReducer
});

export const editingProtocolReducer = combineReducersInOrder(
    initialState,
    newProtocolReducer,
    loadProtocolReducer,
    saveProtocolReducer,
    editActionsReducer);
