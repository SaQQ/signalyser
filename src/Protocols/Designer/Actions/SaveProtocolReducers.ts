import { Action } from "../../../Utils";

import {
    SaveProtocolResult,
    isSaveProtocolPending, isSaveProtocolFulfilled, isSaveProtocolRejected
} from "./SaveProtocol";
import { EditingProtocolState } from "../State";

export function saveProtocolReducer(state: EditingProtocolState, action: Action<SaveProtocolResult>): EditingProtocolState {
    if (isSaveProtocolPending(action)) {
        return { ...state, isLoading: true };
    } else if (isSaveProtocolFulfilled(action)) {
        return {
            isLoading: false, isNew: false,
            id: action.payload.id,
            name: state.name,
            stages: state.stages.map(s => ({ ...s, oldName: s.name })).toList(),
            signals: state.signals.map(s => ({ ...s, oldName: s.name })).toList(),
            tags: state.tags.map(s => ({ ...s, oldName: s.name })).toList(),
            params: state.params.map(s => ({ ...s, oldName: s.name })).toList(),
        };
    } else if (isSaveProtocolRejected(action)) {
        return { ...state, isLoading: false };
    }
    return state;
}
