import { Action, ReduxAction } from "../../../Utils";

interface AddNewParamAction extends ReduxAction {
    type: "AddNewParam";
}

interface RenameParamAction extends ReduxAction {
    type: "RenameParam";
    readonly id: string;
    readonly newName: string;
}

interface RemoveParamAction extends ReduxAction {
    type: "RemoveParam";
    readonly id: string;
}

interface SetParamFormulaAction extends ReduxAction {
    type: "SetParamFormula";
    readonly id: string;
    readonly formula: string;
}

export type ParamActions = AddNewParamAction | RenameParamAction | RemoveParamAction | SetParamFormulaAction;

export const isAddNewParam = (a: Action<AddNewParamAction>): a is AddNewParamAction => a.type === "AddNewParam";
export const isRenameParam = (a: Action<RenameParamAction>): a is RenameParamAction => a.type === "RenameParam";
export const isRemoveParam = (a: Action<RemoveParamAction>): a is RemoveParamAction => a.type === "RemoveParam";
export const isSetParamFormula = (a: Action<SetParamFormulaAction>): a is SetParamFormulaAction => a.type === "SetParamFormula";

export const removeParam = (id: string): RemoveParamAction => ({ type: "RemoveParam", id });
export const addNewParam = (): AddNewParamAction => ({ type: "AddNewParam" });
export const renameParam = (id: string, newName: string): RenameParamAction =>
    ({ type: "RenameParam", id, newName });
export const setParamFormula = (id: string, formula: string): SetParamFormulaAction => ({ type: "SetParamFormula", id, formula });
