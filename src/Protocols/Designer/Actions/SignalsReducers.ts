import { v1 as uuid } from "uuid";
import * as immutable from "immutable";

import { Action, validateNamesCollection, generateUniqueName } from "../../../Utils";

import { SignalActions, isAddNewSignal, isRenameSignal, isRemoveSignal } from "./Signals";

import { SignalsState, EditingSignalState } from "../State";

export function signalsReducer(state: SignalsState, action: Action<SignalActions>): SignalsState {
    if (isAddNewSignal(action)) {
        let name = generateUniqueName("Signal", state);
        return state.push({ id: uuid(), oldName: name, name: name, error: "" });
    } else if (isRenameSignal(action)) {
        let idx = state.findIndex(s => s.id === action.id);
        let newState = idx !== -1 ? state.update(idx, signal => ({ ...signal, name: action.newName })) : state;
        return newState.map(stage => ({ ...stage, error: validateNamesCollection(stage, newState) })).toList();
    } else if (isRemoveSignal(action)) {
        let newState = state.filterNot(e => e.id === action.id).toList();
        return newState.map(stage => ({ ...stage, error: validateNamesCollection(stage, newState) })).toList();
    }
    return state || immutable.List<EditingSignalState>();
}
