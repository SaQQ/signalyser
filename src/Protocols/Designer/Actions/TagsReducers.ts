import * as immutable from "immutable";
import { v1 as uuid } from "uuid";

import { Action, generateUniqueName, validateNamesCollection } from "../../../Utils";

import { TagAction, isAddNewTag, isRenameTag, isRemoveTag } from "./Tags";

import { TagsState, EditingTagState } from "../State";

export function tagsReducer(state: TagsState, action: Action<TagAction>): TagsState {
    if (isAddNewTag(action)) {
        let name = generateUniqueName("Tag", state);
        return state.push({ id: uuid(), oldName: name, name: name, error: "" });
    } else if (isRenameTag(action)) {
        let idx = state.findIndex(s => s.id === action.id);
        let newState = idx !== -1 ? state.update(idx, v => ({ ...v, name: action.newName })) : state;
        return newState.map(tag => ({ ...tag, error: validateNamesCollection(tag, newState) })).toList();
    } else if (isRemoveTag(action)) {
        let newState = state.filterNot(e => e.id === action.id).toList();
        return newState.map(tag => ({ ...tag, error: validateNamesCollection(tag, newState) })).toList();
    }
    return state || immutable.List<EditingTagState>();
}
