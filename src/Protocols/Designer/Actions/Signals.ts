import { Action, ReduxAction } from "../../../Utils";

interface AddNewSignalAction extends ReduxAction {
    type: "AddNewSignal";
}

interface RenameSignalAction extends ReduxAction {
    type: "RenameSignal";
    readonly id: string;
    readonly newName: string;
}

interface RemoveSignalAction extends ReduxAction {
    type: "RemoveSignal";
    readonly id: string;
}

export type SignalActions = AddNewSignalAction | RenameSignalAction;

export const isAddNewSignal = (a: Action<AddNewSignalAction>): a is AddNewSignalAction => a.type === "AddNewSignal";
export const isRenameSignal = (a: Action<RenameSignalAction>): a is RenameSignalAction => a.type === "RenameSignal";
export const isRemoveSignal = (a: Action<RemoveSignalAction>): a is RemoveSignalAction => a.type === "RemoveSignal";

export const addNewSignal = (): AddNewSignalAction => ({ type: "AddNewSignal" });
export const renameSignal = (id: string, newName: string): RenameSignalAction =>
    ({ type: "RenameSignal", id, newName });
export const removeSignal = (id: string): RemoveSignalAction => ({ type: "RemoveSignal", id });
