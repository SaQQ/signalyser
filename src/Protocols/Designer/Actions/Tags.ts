import { Action, ReduxAction } from "../../../Utils";

interface AddNewTagAction extends ReduxAction {
    type: "AddNewTag";
}

interface RenameTagAction extends ReduxAction {
    type: "RenameTag";
    readonly id: string;
    readonly newName: string;
}

interface RemoveTagAction extends ReduxAction {
    type: "RemoveTag";
    readonly id: string;
}

export type TagAction = AddNewTagAction | RenameTagAction | RemoveTagAction;

export const isAddNewTag = (a: Action<AddNewTagAction>): a is AddNewTagAction => a.type === "AddNewTag";
export const isRenameTag = (a: Action<RenameTagAction>): a is RenameTagAction => a.type === "RenameTag";
export const isRemoveTag = (a: Action<RemoveTagAction>): a is RemoveTagAction => a.type === "RemoveTag";

export const addNewTag = (): AddNewTagAction => ({ type: "AddNewTag" });
export const renameTag = (id: string, newName: string): RenameTagAction =>
    ({ type: "RenameTag", id, newName });
export const removeTag = (id: string): RemoveTagAction => ({ type: "RemoveTag", id });
