import { v1 as uuid } from "uuid";
import * as immutable from "immutable";

import { Action, validateNamesCollection, generateUniqueName } from "../../../Utils";

import { StageActions, isAddNewStage, isRenameStage, isRemoveStage } from "./Stages";

import { StagesState, EditingStageState } from "../State";

export function stagesReducer(state: StagesState, action: Action<StageActions>): StagesState {
    if (isAddNewStage(action)) {
        let name = generateUniqueName("Stage", state);
        return state.push({ id: uuid(), oldName: name, name: name, error: "" });
    } else if (isRenameStage(action)) {
        let idx = state.findIndex(s => s.id === action.id);
        let newState = idx !== -1 ? state.update(idx, stage => ({ ...stage, name: action.newName })) : state;
        return newState.map(stage => ({ ...stage, error: validateNamesCollection(stage, newState) })).toList();
    } else if (isRemoveStage(action)) {
        let newState =  state.filterNot(e => e.id === action.id).toList();
        return newState.map(stage => ({ ...stage, error: validateNamesCollection(stage, newState) })).toList();
    }
    return state || immutable.List<EditingStageState>();
}
