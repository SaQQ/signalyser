import * as immutable from "immutable";
import { v1 as uuid } from "uuid";

import * as consts from "../../../Consts";
import { createReducer } from "../../../Utils";
import {
    NewProtocolAction, isNewProtocol,
    ChangeNameAction, isChangeName
} from "./Basic";

import { EditingProtocolState, EditingSignalState, EditingStageState, EditingTagState, EditingParamState } from "../State";

export const changeNameReducer = createReducer("", isChangeName, (_, a) => a.newName);

export function newProtocolReducer(state: EditingProtocolState, action: NewProtocolAction): EditingProtocolState {
    if (isNewProtocol(action)) {
        let tags = consts.DefaultTags.map<EditingTagState>(name => ({ id: uuid(), oldName: name, name, error: "" }));
        return {
            isLoading: false,
            isNew: true,
            id: "",
            name: "New protocol",
            stages: immutable.List<EditingStageState>(),
            signals: immutable.List<EditingSignalState>(),
            tags: immutable.List<EditingTagState>(tags),
            params: immutable.List<EditingParamState>()
        };
    }
    return state;
}
