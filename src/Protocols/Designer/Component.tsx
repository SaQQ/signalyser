import * as React from "react";
import * as immutable from "immutable";

import IconButton from "material-ui/IconButton";
import FlatButton from "material-ui/FlatButton";
import FloatingActionButton from "material-ui/FloatingActionButton";
import TextField from "material-ui/TextField";
import Subheader from "material-ui/Subheader";
import Divider from "material-ui/Divider";
import Paper from "material-ui/Paper";
import { Toolbar, ToolbarGroup } from "material-ui/Toolbar";
import Drawer from "material-ui/Drawer";

import ContentSave from "material-ui/svg-icons/content/save";
import ContentAdd from "material-ui/svg-icons/content/add";
import ActionDelete from "material-ui/svg-icons/action/delete";
import ActionBuild from "material-ui/svg-icons/action/build";

import { grey900, red900 } from "material-ui/styles/colors";

import * as consts from "../../Consts";

import {
    Header, LoadingScreen, StatefulTextField, MonacoEditor, IconPopupButton,
} from "../../Controls";

import { AppState } from "../../State";
import { EditingTagState, EditingParamState, EditingSignalState, EditingStageState } from "./State";
import {
    Dispatch, connect,
    isValidUserId, namedElementExists, Named
} from "../../Utils";

import {
    changeName,
    addNewParam, addNewTag, addNewStage, addNewSignal,
    renameParam, renameTag, renameStage, renameSignal,
    removeParam, removeTag, removeStage, removeSignal,
    setParamFormula,
    saveProtocol
} from "./Actions";

interface NamedWithIdAndError extends Named {
    readonly id: string;
    readonly error: string;
}

interface ElementListProps<T extends NamedWithIdAndError> {
    readonly elements: immutable.List<T>;
    readonly onNameChange: (id: string, newName: string) => void;
    readonly onAdd: () => void;
    readonly onRemove: (id: string) => void;
}

function ElementListTemplate<T extends NamedWithIdAndError>(title: string) {
    return (props: ElementListProps<T>) => <div>
        <Subheader>{title}</Subheader>
        {props.elements.map(e =>
            <div className="element-input" key={e.id}>
                <StatefulTextField
                    id={e.id} value={e.name} fullWidth={true}
                    onValueChange={v => props.onNameChange(e.id, v.toString())}
                    errorText={e.error} />
                <IconButton onTouchTap={() => props.onRemove(e.id)} tooltip="Remove">
                    <ActionDelete />
                </IconButton>
            </div>
        )}
        <div className="add-button">
            <FloatingActionButton mini={true} onTouchTap={props.onAdd}>
                <ContentAdd />
            </FloatingActionButton>
        </div>
    </div>;
}

interface ParamsListProps extends ElementListProps<EditingParamState> {
    readonly onFormulaChange: (id: string, newFormula: string) => void;
}

const ParamsList = (props: ParamsListProps) => <div>
    <Subheader>Parameters</Subheader>
    {props.elements.map(e =>
        <div key={e.id} className="element-input">
            <StatefulTextField
                id={e.id} value={e.oldName} fullWidth={true}
                onValueChange={v => props.onNameChange(e.id, v.toString())}
                errorText={e.error} />
            <IconPopupButton iconType={ActionBuild} tooltip="Edit" iconColor={e.compileError ? red900 : grey900}
                anchorOrigin={{ horizontal: "right", vertical: "center" }}
                targetOrigin={{ horizontal: "left", vertical: "center" }}
                onClosed={() => {}}>
                <div className="param-editor-container">
                    <div style={{ color: grey900, fontSize: consts.FormulaEditorHeaderSize }}>Edit {e.oldName}'s formula</div>
                    {/*<MonacoEditor
                        value={e.formula}
                        containerStyle={{ width: consts.FormulaEditorWidth, height: consts.FormulaEditorHeight, overflow: "hidden" }}
                        options={{ language: "typescript", theme: "vs", selectOnLineNumbers: true }}
                        onDestroyed={(val) => props.onFormulaChange(e.id, val)} />*/}
                    <TextField id={e.id} multiLine fullWidth value={e.formula} onChange={(ev: React.FormEvent<{}>, newValue: string) => props.onFormulaChange(e.id, newValue)}/>
                </div>
            </IconPopupButton>
            <IconButton onTouchTap={() => props.onRemove(e.id)} tooltip="Remove">
                <ActionDelete />
            </IconButton>
        </div>
    )}
    <div className="add-button">
        <FloatingActionButton mini={true} onTouchTap={props.onAdd}>
            <ContentAdd />
        </FloatingActionButton>
    </div>
</div>;

const TagsList = ElementListTemplate<EditingTagState>("Tags");

interface StageProps {
    readonly stage: EditingStageState;
    readonly onNameChange: (id: string, newName: string) => void;
    readonly onRemove: (id: string) => void;
}

const StageHeader = (props: StageProps) =>
    <div className="entry">
        <div>
            <StatefulTextField id={props.stage.id} value={props.stage.oldName} fullWidth={true}
                errorText={props.stage.error} onValueChange={v => props.onNameChange(props.stage.id, v.toString())} />
            <IconButton onTouchTap={() => props.onRemove(props.stage.id)} tooltip="Remove">
                <ActionDelete />
            </IconButton>
        </div>
    </div>;

interface SignalProps {
    readonly signal: EditingSignalState;
    readonly onNameChange: (id: string, newName: string) => void;
    readonly onRemove: (id: string) => void;
}

const SignalHeader = (props: SignalProps) =>
    <div className="entry">
        <div>
            <StatefulTextField id={props.signal.id} value={props.signal.oldName} fullWidth={true}
                errorText={props.signal.error} onValueChange={v => props.onNameChange(props.signal.id, v.toString())} />
            <IconButton onTouchTap={() => props.onRemove(props.signal.id)} tooltip="Remove">
                <ActionDelete />
            </IconButton>
        </div>
    </div>;

interface BoardProps {
    signals: immutable.List<EditingSignalState>;
    stages: immutable.List<EditingStageState>;

    onAddSignal: () => void;
    onAddStage: () => void;
    onSignalNameChange: (id: string, newName: string) => void;
    onStageNameChange: (id: string, newName: string) => void;

    onStageRemove: (id: string) => void;
    onSignalRemove: (id: string) => void;
}

const DesignerBoard = (props: BoardProps) =>
    <div className="protocol-signals">
        <Paper style={{ display: "table-row" }}>
            <div className="entry" key="header">Signals</div>
            {props.stages.map(s => <StageHeader key={s.id} stage={s}
                onNameChange={props.onStageNameChange}
                onRemove={props.onStageRemove} />)}
        </Paper>
        {props.signals.map(signal =>
            <Paper key={signal.id} style={{ display: "table-row" }}>
                <SignalHeader key={signal.id + "-header"} signal={signal}
                    onNameChange={props.onSignalNameChange}
                    onRemove={props.onSignalRemove} />
                {props.stages.map(stage =>
                    <div className="entry" key={signal.id + stage.id}>{signal.name}, {stage.name}</div>
                )}
            </Paper>
        )}
    </div>;

interface DesignerProps {
    readonly isLoading: boolean;
    readonly id: string;
    readonly name: string;
    readonly tags: immutable.List<EditingTagState>;
    readonly params: immutable.List<EditingParamState>;
    readonly signals: immutable.List<EditingSignalState>;
    readonly stages: immutable.List<EditingStageState>;
}

interface DesignerActions {
    onSave: () => void;
    onNameChange: (newName: string) => void;

    onAddTag: () => void;
    onAddParam: () => void;
    onAddSignal: () => void;
    onAddStage: () => void;

    onTagNameChange: (id: string, newName: string) => void;
    onParamNameChange: (id: string, newName: string) => void;
    onParamFormulaChange: (id: string, newFormula: string) => void;
    onSignalNameChange: (id: string, newName: string) => void;
    onStageNameChange: (id: string, newName: string) => void;

    onTagRemove: (id: string) => void;
    onParamRemove: (id: string) => void;
    onStageRemove: (id: string) => void;
    onSignalRemove: (id: string) => void;
}

function ProtocolDesignerControl(props: DesignerProps & DesignerActions) {
    if (props.isLoading) {
        return <LoadingScreen />;
    } else {

        let canSave = props.params.every(p => p.error === "" && p.compileError === false)
            && props.tags.every(t => t.error === "")
            && props.signals.every(s => s.error === "")
            && props.stages.every(s => s.error === "");

        return <div>
            <Header title="Protocol designer">
                <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <StatefulTextField
                            id={props.id}
                            style={consts.TagBarControlStyle}
                            inputStyle={consts.TagBarControlStyle}
                            hintText="Protocol name..."
                            value={props.name}
                            onValueChange={v => props.onNameChange(v.toString())} />
                    </ToolbarGroup>
                    <ToolbarGroup lastChild={true}>
                        <FlatButton onTouchTap={props.onAddSignal} style={consts.TagBarButtonStyle} label="Add signal" />
                        <FlatButton onTouchTap={props.onAddStage} style={consts.TagBarButtonStyle} label="Add stage" />
                        <IconButton disabled={!canSave}
                            onTouchTap={props.onSave} tooltip="Save"><ContentSave {...consts.TagBarControlStyle} /></IconButton>
                    </ToolbarGroup>
                </Toolbar>
            </Header>
            <Drawer docked={true} containerClassName="sidebar-container" containerStyle={{ zIndex: 1000 }} /* Under the AppBar */ >
                <TagsList elements={props.tags}
                    onAdd={props.onAddTag}
                    onRemove={props.onTagRemove}
                    onNameChange={props.onTagNameChange} />
                <ParamsList elements={props.params}
                    onAdd={props.onAddParam}
                    onRemove={props.onParamRemove}
                    onNameChange={props.onParamNameChange}
                    onFormulaChange={props.onParamFormulaChange}
                    />
            </Drawer>
            <div className="content-container">
                <DesignerBoard {...props} />
            </div>
        </div>;
    }
}

const mapProps = (state: AppState): DesignerProps => state.protocols.editing;
const mapActions = (dispatch: Dispatch<AppState>): DesignerActions => ({
    onSave: () => dispatch(saveProtocol()),

    onNameChange: n => dispatch(changeName(n)),

    onAddTag: () => dispatch(addNewTag()),
    onAddParam: () => dispatch(addNewParam()),
    onAddSignal: () => dispatch(addNewSignal()),
    onAddStage: () => dispatch(addNewStage()),

    onTagNameChange: (id, name) => dispatch(renameTag(id, name)),
    onParamNameChange: (id, name) => dispatch(renameParam(id, name)),
    onParamFormulaChange: (id, formula) => dispatch(setParamFormula(id, formula)),
    onSignalNameChange: (id, name) => dispatch(renameSignal(id, name)),
    onStageNameChange: (id, name) => dispatch(renameStage(id, name)),

    onTagRemove: id => dispatch(removeTag(id)),
    onParamRemove: id => dispatch(removeParam(id)),
    onStageRemove: id => dispatch(removeStage(id)),
    onSignalRemove: id => dispatch(removeSignal(id))
});

export const ProtocolDesigner = connect(mapProps, mapActions)(ProtocolDesignerControl);
