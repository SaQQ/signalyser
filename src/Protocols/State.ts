import { combineReducers } from "redux";

import { ProtocolsListState } from "./Selector/State";
import { EditingProtocolState } from "./Designer/State";

export interface ProtocolsState {
    readonly list: ProtocolsListState;
    readonly editing: EditingProtocolState;
}
