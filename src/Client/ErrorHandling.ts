import { remote } from "electron";
import { Dispatch } from "redux";
import { goBack } from "react-router-redux";

import { FetchError } from "./Client";

function showErrorToUser(err: FetchError) {
    console.warn("Server error: ", err);
    if (err.connectionError) {
        remote.dialog.showErrorBox("Cannot contact the server", "Cannot contact server: " + err.message);
    } else if (err.code === 400) {
        remote.dialog.showErrorBox("Invalid data", "Server rejected the data, check if everything is correct.");
    } else {
        remote.dialog.showErrorBox("Server error", "Server was unable to process the request, try again later.");
    }
}

export function handleClientError(err: FetchError) {
    if (err.type === "FetchError") {
        showErrorToUser(err);
        return Promise.resolve(undefined);
    }
    return Promise.reject(err);
}

export function handleAndNavigateBack<S>(dispatch: Dispatch<S>) {
    return (err: FetchError) => {
        if (err.type === "FetchError") {
            showErrorToUser(err);
            dispatch(goBack());
            return Promise.resolve(undefined);
        }
        return Promise.reject(err);
    };
}
