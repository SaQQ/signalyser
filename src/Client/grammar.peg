{
	function identMatcher(ident, tagValue, op) {
        return { [ident]: { [op]: tagValue } };
	}

    function idtrans(idtype) {
        switch(idtype) {
            case "tag":
                return "tags.";
            case "par":
                return "params.";
        }
        return "";
    }
}

start "boolean predicate"
    = disjunctive:disjunctive { return  disjunctive; }
    / _ "all" _ { return {}; }

disjunctive "disjunctive expression"
    = left:conjunctive __ "or" __ right:disjunctive { return { $or: [ left, right ] }; }
	/ conjunctive:conjunctive { return conjunctive;  }

conjunctive "conjunctive expression"
	= left:logical __ "and" __ right:conjunctive { return { $and: [ left, right] }; }
	/ logical:logical { return logical; }

logical "basic logical expression"
	= comparison:comparison { return comparison; }
	/ "(" disjunctive:disjunctive ")" { return disjunctive; }

comparison "comparison"
	= left:identifier _ op:operator _ right:value {
        return identMatcher(left, right, op);
    }

operator "operator"
	= "eq"  { return "$eq"; }
	/ "ne" { return "$ne"; }
	/ "lt"  { return "$lt"; }
	/ "le"  { return "$lte"; }
	/ "gt" { return "$gt"; }
	/ "ge" { return "$gte"; }

value "value"
	 = string:string { return string; }
    / number:number { return parseInt(number); }

number "number literal"
	= sign:([+-]?)integral:([0-9]+)frac:("."([0-9]+))?
    	{
        	return parseFloat((sign || "")
    			+ integral.join("")
				+ (frac === null ? ".0" : ("." + frac[1].join(""))));
		}

// String cannot contain ", no escaping is handled
string "string literal"
	= [\"]chars:([^\"])*[\"] { return chars.join(""); }

identifier "tag or parameter identifier"
    = idtype:( "tag" / "par" ) __ chars:([a-zA-Z0-9]*) { return idtrans(idtype) + chars.join(""); }

// optional whitespace
_  = [ \t\r\n]*

// mandatory whitespace
__ = [ \t\r\n]+

