import "whatwg-fetch"; // This makes VSCode always recognize fetch/BodyInit, does not load anything

import {
    ProtocolDescription, Protocol, ProtocolUpdate,
    ExaminationDescription, Examination, ExaminationWithTags,
    SignalMimeType, SignalData,
    CreatedResult, UpdatedResult, RecomputedResult
} from "../Contract";
import * as binarySignal from "../Parsers/BinarySignal";
import { Settings } from "../Settings/Settings";

export type ProtocolsList = ReadonlyArray<ProtocolDescription>;
export type ExaminationsList = ReadonlyArray<ExaminationDescription>;

export interface FetchError {
    type: "FetchError";
    connectionError: boolean;

    code: number;
    message: string;
}

class ServerClient {

    public loadProtocolsList() {
        return this.get<ProtocolsList>("/protocols");
    }

    public loadProtocol(id: string) {
        return this.get<Protocol>(`/protocols/${id}`);
    }

    public saveProtocol(proto: Protocol) {
        return this.post<CreatedResult>(`/protocols`, JSON.stringify(proto));
    }

    public updateProtocol(proto: ProtocolUpdate) {
        return this.put<UpdatedResult>(`/protocols/${proto.id}`, JSON.stringify(proto));
    }

    public loadExaminationsList(protoId: string, filter: any) {
        return this.post<ExaminationsList>(`/protocols/${protoId}/examinations`, JSON.stringify(filter));
    }

    public loadExamination(protoId: string, examId: string) {
        return this.get<Examination>(`/protocols/${protoId}/examinations/${examId}`);
    }

    public saveExamination(protoId: string, exam: ExaminationWithTags) {
        return this.put<CreatedResult>(`/protocols/${protoId}/examinations`, JSON.stringify(exam));
    }

    public updateExamination(protoId: string, exam: ExaminationWithTags) {
        return this.put<UpdatedResult>(`/protocols/${protoId}/examinations/${exam.id}`, JSON.stringify(exam));
    }

    public recomputeProtocol(protoId: string) {
        return this.post<RecomputedResult>(`/protocols/${protoId}/examinations/recompute`);
    }

    public recomputeExamination(protoId: string, examId: string) {
        return this.postVoid(`/protocols/${protoId}/examinations/${examId}/recompute`);
    }

    public async downloadSignal(protoId: string, examId: string, stage: string, signal: string) {
        let path = `/protocols/${protoId}/examinations/${examId}/signals/${stage}/${signal}`;
        let result = await this.callServer(path, "GET");
        let buff = await result.arrayBuffer();
        return binarySignal.read(buff);
    }

    public uploadSignal(protoId: string, examId: string, stage: string, signal: string, data: SignalData) {
        let path = `/protocols/${protoId}/examinations/${examId}/signals/${stage}/${signal}`;
        let serialized = binarySignal.save(data);
        return this.put<CreatedResult>(path, serialized, SignalMimeType);
    }

    private get<T>(path: string) {
        return this.callServer(path, "GET").then<T>(r => r.json());
    }

    private post<T>(path: string, body?: BodyInit) {
        return this.callServer(path, "POST", body).then<T>(r => r.json());
    }

    private put<T>(path: string, body?: BodyInit, type?: string) {
        return this.callServer(path, "PUT", body, type).then<T>(r => r.json());
    }

    private postVoid(path: string, body?: BodyInit) {
        return this.callServerVoid(path, "POST", body);
    }

    private putVoid(path: string, body?: BodyInit) {
        return this.callServerVoid(path, "PUT", body);
    }

    private callServer(path: string, method: "POST" | "GET" | "PUT" | "DELETE", body?: BodyInit, type?: string) {
        return ServerClient.fixPromise(fetch(Settings.serverAddress + path, {
            method: method,
            body: body,
            headers: {
                [`Content-Type`]: type || `application/json`
            }
        }));
    }

    private callServerVoid(path: string, method: "POST" | "GET" | "PUT" | "DELETE", body?: BodyInit) {
        return ServerClient.fixPromiseVoid(fetch(Settings.serverAddress + path, {
            method: method,
            body: body,
            headers: {
                [`Content-Type`]: `application/json`
            }
        }));
    }

    private static fixPromise(p: Promise<Response>): Promise<Response> {
        return p.then(r => {
            if (!r.ok) {
                return Promise.reject({
                    type: `FetchError`,
                    connectionError: false,
                    code: r.status,
                    message: r.statusText
                });
            }
            return Promise.resolve(r);
        }).catch(r => Promise.reject({
            type: `FetchError`,
            connectionError: true,
            code: 0,
            message: r.message
        }));
    }

    private static fixPromiseVoid(p: Promise<Response>): Promise<void> {
        return p.then<void>(r => {
            if (!r.ok) {
                return Promise.reject({
                    type: `FetchError`,
                    connectionError: false,
                    code: r.status,
                    message: r.statusText
                });
            }
            return Promise.resolve();
        }).catch(r => Promise.reject({
            type: `FetchError`,
            connectionError: true,
            code: 0,
            message: r.message
        }));
    }
}

const instance = new ServerClient();
export { instance as ServerClient };
