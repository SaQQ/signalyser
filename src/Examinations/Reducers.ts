import { combineReducers } from "redux";

import { ExaminationsState } from "./State";
import { examinationsListReducer } from "./Selector/Actions/Reducers";
import { editingExaminationsReducer } from "./Editor/Actions/Reducers";

export const examinationsReducer = combineReducers<ExaminationsState>({
    ["list"]: examinationsListReducer,
    ["editing"]: editingExaminationsReducer,
});
