import * as immutable from "immutable";
import { writeFileSync } from "fs";

import { Action } from "../../../Utils";

import { FilterAction, isChangeFilter } from "./Filter";

import { ExaminationsListState } from "../State";

export function filterReducer(state: ExaminationsListState, action: Action<FilterAction>): ExaminationsListState {
    if (isChangeFilter(action)) {
        return {  ...state, filterText: action.filterText, filter: action.filter, filterError: action.errorText };
    }
    return state;
}
