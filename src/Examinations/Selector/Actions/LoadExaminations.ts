import {
    Action, ThunkAction, PromiseAction,
    FulfilledPromiseAction, RejectedPromiseAction
} from "../../../Utils";
import { ServerClient, ExaminationsList, handleAndNavigateBack } from "../../../Client";

import { AppState } from "../../../State";

export interface LoadExaminationsAction extends PromiseAction<ExaminationsList> {
    type: "LoadExaminations";
}
export interface LoadExaminationsPendingAction extends PromiseAction<ExaminationsList> {
    type: "LoadExaminations_PENDING";
}
export interface LoadExaminationsFulfilledAction extends FulfilledPromiseAction<ExaminationsList> {
    type: "LoadExaminations_FULFILLED";
}
export interface LoadExaminationsRejectedAction extends RejectedPromiseAction<ExaminationsList> {
    type: "LoadExaminations_REJECTED";
}

export const isLoadExaminations =
    (a: Action<LoadExaminationsAction>): a is LoadExaminationsAction => a.type === "LoadExaminations";
export const isLoadExaminationsPending =
    (a: Action<LoadExaminationsPendingAction>): a is LoadExaminationsPendingAction => a.type === "LoadExaminations_PENDING";
export const isLoadExaminationsFulfilled =
    (a: Action<LoadExaminationsFulfilledAction>): a is LoadExaminationsFulfilledAction => a.type === "LoadExaminations_FULFILLED";
export const isLoadExaminationsRejected =
    (a: Action<LoadExaminationsRejectedAction>): a is LoadExaminationsRejectedAction => a.type === "LoadExaminations_REJECTED";

export type LoadExaminationsResult =
    LoadExaminationsAction
    | LoadExaminationsPendingAction
    | LoadExaminationsFulfilledAction
    | LoadExaminationsRejectedAction;

export function loadExaminations(protoId: string, filter: any): ThunkAction<AppState> {
    return dispatch => dispatch({
        type: "LoadExaminations",
        payload: ServerClient.loadExaminationsList(protoId, filter)
    }).catch(handleAndNavigateBack(dispatch));
}
