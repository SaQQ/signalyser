import {
    Action, ThunkAction,
    PromiseAction, FulfilledPromiseAction, RejectedPromiseAction
} from "../../../Utils";

import { ServerClient, handleAndNavigateBack } from "../../../Client";

import { RecomputedResult } from "../../../Contract";

import { AppState } from "../../../State";

export interface RecomputeProtocolAction extends PromiseAction<RecomputedResult> {
    type: "RecomputeProtocol";
}

export interface RecomputeProtocolPendingAction extends PromiseAction<RecomputedResult> {
    type: "RecomputeProtocol_PENDING";
}

export interface RecomputeProtocolFulfilledAction extends PromiseAction<RecomputedResult> {
    type: "RecomputeProtocol_FULFILLED:";
}

export interface RecomputeProtocolRejectedAction extends PromiseAction<RecomputedResult> {
    type: "RecomputeProtocol_REJECTED";
}

export const isRecomputeProtocol =
    (a: Action<RecomputeProtocolAction>): a is RecomputeProtocolAction => a.type === "RecomputeProtocol";
export const isRecomputeProtocolPending =
    (a: Action<RecomputeProtocolPendingAction>): a is RecomputeProtocolPendingAction => a.type === "RecomputeProtocol_PENDING";
export const isRecomputeProtocolFulfilled =
    (a: Action<RecomputeProtocolFulfilledAction>): a is RecomputeProtocolFulfilledAction => a.type === "RecomputeProtocol_FULFILLED";
export const isRecomputeProtocolRejected =
    (a: Action<RecomputeProtocolRejectedAction>): a is RecomputeProtocolRejectedAction => a.type === "RecomputeProtocol_REJECTED";

export type RecomputeProtocolResult =
    RecomputeProtocolAction
    | RecomputeProtocolPendingAction
    | RecomputeProtocolFulfilledAction
    | RecomputeProtocolRejectedAction;

export function recomputeProtocol(protoId: string): ThunkAction<AppState> {
    return dispatch => dispatch({
            type: "RecomputeProtocol",
            payload: ServerClient.recomputeProtocol(protoId)
    }).catch(handleAndNavigateBack(dispatch));
}
