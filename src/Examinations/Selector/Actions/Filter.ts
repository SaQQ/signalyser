import { Action, ReduxAction } from "../../../Utils";

export interface ChangeFilterAction extends ReduxAction {
    type: "ChangeFilter";
    readonly filterText: string;
    readonly filter: Object;
    readonly errorText: string;
}

export const isChangeFilter = (a: Action<ChangeFilterAction>): a is ChangeFilterAction => a.type === "ChangeFilter";

export type FilterAction = ChangeFilterAction;

export const changeFilter = (filterText: string, filter: Object, errorText: string): ChangeFilterAction =>
    ({ type: "ChangeFilter", filterText, filter, errorText });
