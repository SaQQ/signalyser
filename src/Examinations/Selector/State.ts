import * as immutable from "immutable";

import { ExaminationDescription } from "../../Contract";

export type ExaminationsList = immutable.List<ExaminationDescription>;
export function ExaminationsList(arr?: ReadonlyArray<ExaminationDescription>) {
    if (arr === undefined) {
        return immutable.List<ExaminationDescription>();
    } else {
        return immutable.List<ExaminationDescription>(arr);
    }
}

export interface ExaminationsListState {
    readonly isLoading: boolean;
    readonly loadedExaminations: ExaminationsList;
    readonly filterText: string;
    readonly filterError: string;
    readonly filter: Object;
}

export const emptyExaminationsListState: ExaminationsListState = {
    isLoading: false,
    loadedExaminations: ExaminationsList(),
    filterText: "all",
    filterError: "",
    filter: {}
};
