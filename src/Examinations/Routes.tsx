import * as React from "react";
import { Route } from "react-router";

import { SelectExamination, EditExamination } from "./Navigation";

import { ExaminationSelector } from "./Selector/Component";
import { ExaminationEditor } from "./Editor/Component";

export const Routes = [
    <Route key={SelectExamination} path={SelectExamination} component={ExaminationSelector} />,
    <Route key={EditExamination} path={EditExamination} component={ExaminationEditor} />
];
