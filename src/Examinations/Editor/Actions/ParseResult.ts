import { Action, ReduxAction, } from "../../../Utils";

export interface CloseParseResultAction extends ReduxAction {
    type: "CloseParseResult";
}

export interface SubmitParseResultAction extends ReduxAction {
    type: "SubmitParseResult";
}

export interface SelectSignalAction extends ReduxAction {
    type: "SelectSignal";
    index: number;
}

export interface SelectPlaceAction extends ReduxAction {
    type: "SelectPlace";
    signal: string;
    stage: string;
}

export type ParseResultAction = CloseParseResultAction | SubmitParseResultAction | SelectPlaceAction | SelectSignalAction;

export const isCloseParseResult = (a: Action<CloseParseResultAction>): a is CloseParseResultAction => a.type === "CloseParseResult";
export const isSubmitParseResult = (a: Action<SubmitParseResultAction>): a is SubmitParseResultAction => a.type === "SubmitParseResult";
export const isSelectSignalAction = (a: Action<SelectSignalAction>): a is SelectSignalAction => a.type === "SelectSignal";
export const isSelectPlaceAction = (a: Action<SelectPlaceAction>): a is SelectPlaceAction => a.type === "SelectPlace";

export const closeParseResult = (): CloseParseResultAction => ({ type: "CloseParseResult" });
export const submitParseResult = (): SubmitParseResultAction => ({ type: "SubmitParseResult" });
export const selectSignal = (index: number): SelectSignalAction => ({ type: "SelectSignal", index });
export const selectPlace = (signal: string, stage: string): SelectPlaceAction => ({ type: "SelectPlace", signal, stage });
