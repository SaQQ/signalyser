import { Action } from "../../../Utils";

import {
    SaveExaminationResult,
    isSaveExaminationPending, isSaveExaminationFulfilled, isSaveExaminationRejected
} from "./SaveExamination";
import { EditingExaminationState } from "../State";

export function saveExaminationReducer(state: EditingExaminationState, action: Action<SaveExaminationResult>): EditingExaminationState {
    if (isSaveExaminationPending(action)) {
        return { ...state, isLoading: true };
    } else if (isSaveExaminationFulfilled(action)) {
        return { ...state, isLoading: false, isNew: false, id: action.payload.id };
    } else if (isSaveExaminationRejected(action)) {
        return { ...state, isLoading: false };
    }
    return state;
}
