import { Action, ReduxAction } from "../../../Utils";

export interface SetTagValueAction extends ReduxAction {
    type: "SetTagValue";
    readonly name: string;
    readonly newValue: string;
}

export interface AddTagValueAction extends ReduxAction {
    type: "AddTagValue";
    readonly name: string;
    readonly value: string;
}

export const isSetTagValue = (a: Action<SetTagValueAction>): a is SetTagValueAction => a.type === "SetTagValue";
export const isAddTagValue = (a: Action<AddTagValueAction>): a is AddTagValueAction => a.type === "AddTagValue";

export type TagAction = SetTagValueAction | AddTagValueAction;

export const setTagValue = (name: string, newValue: string): SetTagValueAction =>
    ({ type: "SetTagValue", name, newValue });

export const addTagValue = (name: string, value: string): AddTagValueAction =>
    ({ type: "AddTagValue", name, value });
