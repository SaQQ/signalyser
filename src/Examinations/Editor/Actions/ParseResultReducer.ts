import { List } from "immutable";

import { Action } from "../../../Utils";

import { ParseResultAction, isCloseParseResult, isSubmitParseResult, isSelectSignalAction, isSelectPlaceAction } from "./ParseResult";

import { ParseResultState, EditingExaminationState } from "../State";

function isPlaceSelected(state: ParseResultState) {
    return state.selectedPlace[0] !== "" || state.selectedPlace[1] !== "";
}

function isSignalSelected(state: ParseResultState) {
    return state.selectedSignal !== -1;
}

export function parseResultReducer(state: ParseResultState, action: Action<ParseResultAction>): ParseResultState {
    if (isCloseParseResult(action)) {
        return { ...state, displayingParseResult: false };
    } else if (isSelectPlaceAction(action)) {
        if (isSignalSelected(state)) {
            // Previously selected signal is to be bound with selected place
            // Selected place can be bound with another signal - unbound it

            let prevSignalIndex = state.signalAssignment.findIndex(
                signal => signal.signal === action.signal && signal.stage === action.stage);

            let newSignals = state.signalAssignment.update(state.selectedSignal, signal => ({
                ...signal,
                signal: action.signal,
                stage: action.stage
            }));

            if (prevSignalIndex !== -1) {
                newSignals = newSignals.update(prevSignalIndex, signal => ({
                    ...signal,
                    signal: "",
                    stage: ""
                }));
            }

            return {
                ...state, selectedPlace: ["", ""], selectedSignal: -1,
                signalAssignment: newSignals
            };

        } else if (isPlaceSelected(state)) {
            if (state.selectedPlace[0] === action.signal && state.selectedPlace[1] === action.stage) {
                // Selected same place twice
                return { ...state, selectedPlace: ["", ""] };
            } else {
                // Selected another place - clear its assignment

                let idx = state.signalAssignment.findIndex(signal => signal.signal === action.signal && signal.stage === action.stage);
                if (idx !== -1) {
                    // Place was assigned
                    return {
                        ...state, selectedPlace: [action.signal, action.stage],
                        signalAssignment: state.signalAssignment.update(idx, signal => ({ ...signal, signal: "", stage: "" }))
                    };
                } else {
                    return { ...state, selectedPlace: [action.signal, action.stage] };
                }
            }
        } else {
            // Nothing was selected and selected place - clear its assignment

            let idx = state.signalAssignment.findIndex(signal => signal.signal === action.signal && signal.stage === action.stage);
            if (idx !== -1) {
                // Place was assigned
                return {
                    ...state, selectedPlace: [action.signal, action.stage],
                    signalAssignment: state.signalAssignment.update(idx, signal => ({ ...signal, signal: "", stage: "" }))
                };
            } else {
                return { ...state, selectedPlace: [action.signal, action.stage] };
            }
        }
    } else if (isSelectSignalAction(action)) {
        if (isPlaceSelected(state)) {
            // Previously selected place is to be bound with selected signal

            return {
                ...state, selectedSignal: -1, selectedPlace: ["", ""],
                signalAssignment: state.signalAssignment.update(action.index,
                    signal => ({ ...signal, signal: state.selectedPlace[0], stage: state.selectedPlace[1] }))
            };
        } else if (isSignalSelected(state)) {
            if (state.selectedSignal === action.index) {
                // Selected same signal twice
                return { ...state, selectedSignal: -1 };
            } else {
                // Selected another signal - clear its assignment
                return {
                    ...state, selectedSignal: action.index, signalAssignment: state.signalAssignment.update(action.index,
                        signal => ({ ...signal, signal: "", stage: "" }))
                };
            }
        } else {
            // Nothing was selected and selected signal - clear its assignment
            return {
                ...state, selectedSignal: action.index, signalAssignment: state.signalAssignment.update(action.index,
                    signal => ({ ...signal, signal: "", stage: "" }))
            };
        }
    }
    return state || { displayingParseResult: false, parserError: "", selectedPlace: ["", ""], selectedSignal: -1, signalAssignment: List([]), tagsAssignment: {} };
}

export function submitParseResultReducer(state: EditingExaminationState, action: Action<ParseResultAction>): EditingExaminationState {
    if (isSubmitParseResult(action)) {

        let { signalAssignment, tagsAssignment } = state.parseResult;

        let signals = state.signals;
        let tags = state.tags;

        signalAssignment.filter(signal => signal.signal !== "" && signal.stage !== "")
            .map(signal => ({ signal: signal.signal, stage: signal.stage, data: signal.data }))
            .forEach(signal => {
                let idx = signals.findIndex(s => s.signal === signal.signal && s.stage === signal.stage);
                if (idx !== -1) {
                    // Replace existing signal
                    signals = signals.update(idx, _ => signal);
                } else {
                    // Append signal
                    signals = signals.push(signal);
                }
            });

        return {
            ...state, signals: signals, tags: Object.assign(tags, tagsAssignment),
            parseResult: { ...state.parseResult, displayingParseResult: false },
        };
    }
    return state;
}
