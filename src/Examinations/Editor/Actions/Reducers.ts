import * as immutable from "immutable";
import {
    combineReducers, combineReducersInOrder, idReducer
} from "../../../Utils";

import { EditingExaminationState, emptyEditingExaminationsState, emptyProtocol } from "../State";

import { tagsReducer } from "./TagsReducers";

import { loadExaminationReducer } from "./LoadExaminationReducers";
import { saveExaminationReducer } from "./SaveExaminationReducers";
import { newExaminationReducer } from "./NewExaminationReducers";
import { parseFileReducer } from "./ParseFileReducer";
import { parserChooserReducer } from "./ParserChooserReducers";
import { parseResultReducer, submitParseResultReducer } from "./ParseResultReducer";

const initialState = idReducer(emptyEditingExaminationsState);

const editActionsReducer = combineReducers<EditingExaminationState>({
    ["isLoading"]: idReducer(false),
    ["isNew"]: idReducer(false),

    ["id"]: idReducer(""),
    ["protocol"]: idReducer(emptyProtocol),

    ["signals"]: idReducer(immutable.List<any>()),
    ["tags"]: tagsReducer,
    ["params"]: idReducer({}),

    ["choosingParser"]: parserChooserReducer,

    ["parseResult"]: parseResultReducer,
});

export const editingExaminationsReducer = combineReducersInOrder(
    initialState,
    loadExaminationReducer,
    saveExaminationReducer,
    newExaminationReducer,
    editActionsReducer,
    parseFileReducer,
    submitParseResultReducer
);
