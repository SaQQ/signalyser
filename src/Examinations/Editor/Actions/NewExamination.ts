import { remote } from "electron";
import {
    Action, ThunkAction,
    PromiseAction, PendingPromiseAction, FulfilledPromiseAction, RejectedPromiseAction,
} from "../../../Utils";
import { ServerClient, handleAndNavigateBack } from "../../../Client";

import { AppState } from "../../../State";
import { Protocol } from "../../../Contract";

interface NewExaminationAction extends PromiseAction<Protocol> {
    type: "NewExamination";
}
interface NewExaminationPendingAction extends PendingPromiseAction<Protocol> {
    type: "NewExamination_PENDING";
}
interface NewExaminationFulfilledAction extends FulfilledPromiseAction<Protocol> {
    type: "NewExamination_FULFILLED";
}
interface NewExaminationRejectedAction extends RejectedPromiseAction<Protocol> {
    type: "NewExamination_REJECTED";
}
export const isNewExamination =
    (a: Action<NewExaminationAction>): a is NewExaminationAction => a.type === "NewExamination";
export const isNewExaminationPending =
    (a: Action<NewExaminationPendingAction>): a is NewExaminationPendingAction => a.type === "NewExamination_PENDING";
export const isNewExaminationFulfilled =
    (a: Action<NewExaminationFulfilledAction>): a is NewExaminationFulfilledAction => a.type === "NewExamination_FULFILLED";
export const isNewExaminationRejected =
    (a: Action<NewExaminationRejectedAction>): a is NewExaminationRejectedAction => a.type === "NewExamination_REJECTED";

export type NewExaminationResult = NewExaminationPendingAction | NewExaminationFulfilledAction | NewExaminationRejectedAction;

export function newExamination(protoId: string): ThunkAction<AppState> {
    return dispatch => dispatch({
        type: "NewExamination",
        payload: ServerClient.loadProtocol(protoId)
    }).catch(handleAndNavigateBack);
}
