import * as immutable from "immutable";

import { Action } from "../../../Utils";

import {
    LoadExaminationResult,
    isLoadExaminationPending, isLoadExaminationFulfilled, isLoadExaminationRejected
} from "./LoadExamination";
import { EditingExaminationState, RealSignal } from "../State";

export function loadExaminationReducer(state: EditingExaminationState, action: Action<LoadExaminationResult>): EditingExaminationState {
    if (isLoadExaminationPending(action)) {
        return { ...state, isLoading: true };
    } else if (isLoadExaminationFulfilled(action)) {
        let [protocol, exam, signals] = action.payload;
        return {
            ...state,
            isLoading: false,
            isNew: false,

            id: exam.id,
            protocol: protocol,

            tags: exam.tags,
            signals: immutable.List<RealSignal>(signals),
            params: exam.params
        };
    } else if (isLoadExaminationRejected(action)) {
        return { ...state, isLoading: false };
    }
    return state;
}
