import { remote } from "electron";

import {
    Action, ThunkAction,
    PromiseAction, PendingPromiseAction, FulfilledPromiseAction, RejectedPromiseAction,
} from "../../../Utils";

import { ParserReturnType } from "../../../Parsers";

import { AppState } from "../../../State";

import { ParserRegisterEntry, Parse } from "../../../Parsers";

interface ParseFileAction extends PromiseAction<ParserReturnType> {
    type: "ParseFile";
}
interface ParseFilePendingAction extends PendingPromiseAction<ParserReturnType> {
    type: "ParseFile_PENDING";
}
interface ParseFileFulfilledAction extends FulfilledPromiseAction<ParserReturnType> {
    type: "ParseFile_FULFILLED";
}
interface ParseFileRejectedAction extends RejectedPromiseAction<ParserReturnType> {
    type: "ParseFile_REJECTED";
}
export const isParseFile =
    (a: Action<ParseFileAction>): a is ParseFileAction => a.type === "ParseFile";
export const isParseFilePending =
    (a: Action<ParseFilePendingAction>): a is ParseFilePendingAction => a.type === "ParseFile_PENDING";
export const isParseFileFulfilled =
    (a: Action<ParseFileFulfilledAction>): a is ParseFileFulfilledAction => a.type === "ParseFile_FULFILLED";
export const isParseFileRejected =
    (a: Action<ParseFileRejectedAction>): a is ParseFileRejectedAction => a.type === "ParseFile_REJECTED";

export type ParseFileResult = ParseFilePendingAction | ParseFileFulfilledAction | ParseFileRejectedAction;

function openFileAsPromised(title: string, filters: { name: string; extensions: string[] }[]) {
    return new Promise<string>((resolve, reject) => {
        remote.dialog.showOpenDialog({ title, filters }, f => {
            if (f !== undefined) {
                resolve(f[0]);
            } else {
                reject(new Error("No file selected"));
            }
        });
    });
}

async function loadAndParse(parser: ParserRegisterEntry) {
    try {
        const exts = parser.extensions.map(ext => ({
            name: "." + ext + " files",
            extensions: [ext]
        }));
        let file = await openFileAsPromised(parser.name, exts);
        return await Parse(file, parser.parser);
    } catch (e) {
        throw e.message;
    }
}

export function parseFile(parser: ParserRegisterEntry): ThunkAction<AppState> {
    return dispatch => dispatch({
        type: "ParseFile",
        payload: loadAndParse(parser)
    }).catch(() => { });
}
