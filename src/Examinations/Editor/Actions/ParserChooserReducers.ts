import { Action } from "../../../Utils";

import { ParserChooserAction, isOpenParserChooser, isCloseParserChooser } from "./ParserChooser";

import { EditingExaminationState } from "../State";

export function parserChooserReducer(state: boolean, action: Action<ParserChooserAction>): boolean {
    if (isOpenParserChooser(action)) {
        return true;
    } else if (isCloseParserChooser(action)) {
        return false;
    }
    return state || false;
}
