import * as immutable from "immutable";
import { v1 as uuid } from "uuid";

import { Protocol, Examination, SignalData, AttributesCollection, TagValue } from "../../Contract";

import { ParserReturnType, SignalAssignment, } from "../../Parsers";

export { AttributesCollection } from "../../Contract";

export type TagsState = AttributesCollection<TagValue>;
export type SignalsState = immutable.List<RealSignal>;
export type SignalAssignmentState = immutable.List<SignalAssignment>;

export interface RealSignal {
    readonly stage: string;
    readonly signal: string;
    readonly data: SignalData;
}

export interface ParseResultState {
    readonly tagsAssignment: TagsState;
    readonly signalAssignment: SignalAssignmentState;
    readonly parserError: string;
    readonly displayingParseResult: boolean;
    readonly selectedSignal: number;
    readonly selectedPlace: [string, string];
}

export interface EditingExaminationState {
    readonly isLoading: boolean;
    readonly isNew: boolean;

    readonly id: string;
    readonly protocol: Protocol;
    readonly signals: SignalsState;
    readonly tags: TagsState;
    readonly params: AttributesCollection<Object>;

    readonly choosingParser: boolean;
    readonly parseResult: ParseResultState;
}

export const emptyProtocol: Protocol = {
    id: "",
    name: "",
    signals: [],
    stages: [],
    tags: [],
    params: []
};

export const emptyEditingExaminationsState: EditingExaminationState = {
    isLoading: false,
    isNew: false,

    protocol: emptyProtocol,
    id: "",

    signals: immutable.List<RealSignal>(),
    tags: {},
    params: {},

    choosingParser: false,

    parseResult: {
        displayingParseResult: false,
        tagsAssignment: {},
        signalAssignment: immutable.List([]),
        selectedPlace: ["", ""],
        selectedSignal: -1,
        parserError: ""
    }
};
