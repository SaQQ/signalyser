import * as React from "react";
import * as immutable from "immutable";

import IconButton from "material-ui/IconButton";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import FloatingActionButton from "material-ui/FloatingActionButton";
import TextField from "material-ui/TextField";
import Subheader from "material-ui/Subheader";
import Divider from "material-ui/Divider";
import Paper from "material-ui/Paper";
import { Toolbar, ToolbarGroup } from "material-ui/Toolbar";
import Drawer from "material-ui/Drawer";
import Dialog from "material-ui/Dialog";
import { List, ListItem } from "material-ui/List";
import { grey50, cyan400, green400 } from "material-ui/styles/colors";

import ContentSave from "material-ui/svg-icons/content/save";
import ContentAdd from "material-ui/svg-icons/content/add";
import ActionDelete from "material-ui/svg-icons/action/delete";

import * as consts from "../../Consts";

import { Header, LoadingScreen, StatefulTextField } from "../../Controls";
import { ExaminationPlot } from "../../Controls";

import { AppState } from "../../State";
import { RealSignal, AttributesCollection } from "./State";
import { Dispatch, connect } from "../../Utils";

import { AvalaibleParsers, ParserRegisterEntry, SignalAssignment } from "../../Parsers";

import {
    setTagValue, addTagValue, saveExamination, parseFile,
    openParserChooser, closeParserChooser,
    closeParseResult, submitParseResult,
    selectSignal, selectPlace
} from "./Actions";

import {
    Protocol, SignalDescriptor, StageDescriptor, TagDescriptor, TagValue
} from "../../Contract";

interface NameValueElement {
    readonly name: string;
    readonly value: string;
}

interface ElementListProps<T extends NameValueElement> {
    readonly elements: immutable.List<T>;
}

function ElementList<T extends NameValueElement>(title: string) {
    return (props: ElementListProps<T>) => <div>
        <Subheader>{title}</Subheader>
        {props.elements.map(e =>
            <div key={e.name} className="element-input">
                <TextField id={e.name} value={e.value} fullWidth={true}
                    floatingLabelText={e.name}
                    disabled={true} />
            </div>
        )}
    </div>;
}

interface SettableElementListProps<T extends NameValueElement> {
    readonly elements: immutable.List<T>;
    readonly onSetValue: (name: string, newValue: string) => void;
}

function SettableElementList<T extends NameValueElement>(title: string) {
    return (props: SettableElementListProps<T>) => <div>
        <Subheader>{title}</Subheader>
        {props.elements.map(e =>
            <div key={e.name} className="element-input">
                <StatefulTextField
                    id={e.name} value={e.value} fullWidth={true}
                    floatingLabelText={e.name}
                    onValueChange={v => props.onSetValue(e.name, v.toString())} />
            </div>
        )}
    </div>;
}

interface ParamEntry extends NameValueElement { }

interface TagEntry extends NameValueElement { }

const TagsList = SettableElementList<TagEntry>("Tags");
const UnsetTagsList = SettableElementList<TagEntry>("Unset tags");

const ParamsList = ElementList<ParamEntry>("Parameters");

interface SignalsBoardProps {
    protocolSignals: ReadonlyArray<SignalDescriptor>;
    protocolStages: ReadonlyArray<StageDescriptor>;
    data: immutable.List<RealSignal>;
}

const SignalsBoard = (props: SignalsBoardProps) => {
    let data = props.data;
    return <Paper zDepth={2} style={{ width: "100%", height: "100%" }}>
        <ExaminationPlot {...props} />
    </Paper>;
};

interface ParseResultBoardProps {
    readonly displayingParseResult: boolean;
    readonly parserError: string;

    readonly protocol: Protocol;

    readonly tagsAssignment: AttributesCollection<TagValue>;
    readonly signalAssignment: immutable.List<SignalAssignment>;

    readonly selectedSignal: number;
    readonly selectedPlace: [string, string];
}

interface ParseResutlBoardActions {
    closeParseResult: () => void;
    submitParseResult: () => void;

    onSelectSignal: (index: number) => void;
    onSelectPlace: (signal: string, stage: string) => void;
}

const ParseResultBoard = (props: ParseResultBoardProps & ParseResutlBoardActions) => {
    return <Dialog title="Parse result" open={props.displayingParseResult} onRequestClose={props.closeParseResult}
        actions={[<FlatButton onTouchTap={props.parserError === "" ? props.submitParseResult : props.closeParseResult} label="Submit" />]}>
        {props.parserError === "" ?
            <div className="parse-result-container">
                <List>
                    <Subheader>Tags</Subheader>
                    {Object.keys(props.tagsAssignment).map((tagName, i) => <ListItem key={i}>{tagName + ": " + props.tagsAssignment[tagName].toString()}</ListItem>)}
                </List>
                <List>
                    <Subheader>Signals</Subheader>
                    {
                        props.signalAssignment.map((signal, i) => <ListItem key={i}
                            style={{ backgroundColor: props.selectedSignal === i ? cyan400 : (signal.signal !== "" && signal.stage !== "" ? green400 : grey50) }}
                            onTouchTap={() => props.onSelectSignal(i)} >
                            {signal.description}
                        </ListItem>)
                    }
                </List>
                <table className="proto-table">
                    <tbody>
                        <tr key="header">
                            <td />
                            {props.protocol.stages.map(s => <td key={s.name}>{s.name}</td>)}
                        </tr>
                        {props.protocol.signals.map(signal =>
                            <tr key={signal.name}>
                                <td>{signal.name}</td>
                                {props.protocol.stages.map(stage => {
                                    let assignment = props.signalAssignment.find(s => s.signal === signal.name && s.stage === stage.name);
                                    return <td key={signal.name + stage.name}>
                                        <RaisedButton backgroundColor={props.selectedPlace[0] === signal.name && props.selectedPlace[1] === stage.name ? cyan400 : (assignment !== undefined ? green400 : grey50)} style={{ width: "100%" }}
                                            onTouchTap={() => props.onSelectPlace(signal.name, stage.name)} label={assignment !== undefined ? assignment.description : " "} />
                                    </td>;
                                })}
                            </tr>
                        )}
                    </tbody>
                </table>
            </div> : <span>{props.parserError}</span>
        }
    </Dialog>;
};

interface EditorProps {
    readonly isLoading: boolean;

    readonly id: string;
    readonly protocol: Protocol;
    readonly tags: AttributesCollection<TagValue>;
    readonly signals: immutable.List<RealSignal>;
    readonly params: AttributesCollection<Object>;

    readonly choosingParser: boolean;

    readonly tagsAssignment: AttributesCollection<TagValue>;
    readonly signalAssignment: immutable.List<SignalAssignment>;
    readonly parserError: string;
    readonly displayingParseResult: boolean;
    readonly selectedSignal: number;
    readonly selectedPlace: [string, string];
}

interface EditorActions {
    onSave: () => void;

    onSetTagValue: (name: string, newValue: string) => void;
    onAddTagValue: (name: string, value: string) => void;

    openParserChooser: () => void;
    closeParserChooser: () => void;
    parseFile: (parser: ParserRegisterEntry) => void;
    closeParseResult: () => void;
    submitParseResult: () => void;

    onSelectSignal: (index: number) => void;
    onSelectPlace: (signal: string, stage: string) => void;
}

function ExaminationEditorControl(props: EditorProps & EditorActions) {

    if (props.isLoading) {
        return <LoadingScreen />;
    } else {
        let setTags = immutable.List(Object.keys(props.tags).map(name => ({ name, value: props.tags[name].toString() })));

        let unsetTags = immutable.List(props.protocol.tags
            .filter(t => Object.keys(props.tags).find(tagName => tagName === t.name) === undefined)
            .map(t => ({ name: t.name, value: "" })));

        let params = immutable.List(Object.keys(props.params).map(name => ({ name, value: JSON.stringify(props.params[name]) })));

        return <div>
            <Header title="Examination editor">
                <Toolbar>
                    <ToolbarGroup lastChild={true}>
                        <FlatButton style={consts.TagBarButtonStyle} onTouchTap={props.openParserChooser} label="Load data" />
                        <IconButton onTouchTap={props.onSave}><ContentSave {...consts.TagBarControlStyle} /></IconButton>
                    </ToolbarGroup>
                </Toolbar>
            </Header>
            <Drawer docked={true} containerClassName="sidebar-container" containerStyle={{ zIndex: 1000 }} /* Under the AppBar */ >
                <TagsList elements={setTags}
                    onSetValue={props.onSetTagValue} />
                <Divider />
                <UnsetTagsList elements={unsetTags}
                    onSetValue={props.onAddTagValue} />
                <Divider />
                <ParamsList elements={immutable.List(params)} />
            </Drawer>
            <Dialog open={props.choosingParser} onRequestClose={props.closeParserChooser} title="Choose parser">
                {
                    AvalaibleParsers.map((p, i) => <RaisedButton key={i} label={p.name} onTouchTap={() => props.parseFile(p)} />)
                }
            </Dialog>
            <ParseResultBoard {...props} />
            <div className="content-container">
                <SignalsBoard protocolSignals={props.protocol.signals}
                    protocolStages={props.protocol.stages}
                    data={props.signals} />
            </div>
        </div>;
    }
}

const mapProps = (state: AppState): EditorProps => ({
    isLoading: state.examinations.editing.isLoading,

    id: state.examinations.editing.id,
    protocol: state.examinations.editing.protocol,
    tags: state.examinations.editing.tags,
    signals: state.examinations.editing.signals,
    params: state.examinations.editing.params,

    choosingParser: state.examinations.editing.choosingParser,

    tagsAssignment: state.examinations.editing.parseResult.tagsAssignment,
    signalAssignment: state.examinations.editing.parseResult.signalAssignment,
    displayingParseResult: state.examinations.editing.parseResult.displayingParseResult,
    parserError: state.examinations.editing.parseResult.parserError,
    selectedPlace: state.examinations.editing.parseResult.selectedPlace,
    selectedSignal: state.examinations.editing.parseResult.selectedSignal
});

const mapActions = (dispatch: Dispatch<AppState>): EditorActions => ({
    onSave: () => dispatch(saveExamination()),

    onSetTagValue: (name, newValue) => dispatch(setTagValue(name, newValue)),
    onAddTagValue: (name, value) => dispatch(addTagValue(name, value)),

    openParserChooser: () => dispatch(openParserChooser()),
    closeParserChooser: () => dispatch(closeParserChooser()),
    parseFile: (parser) => dispatch(parseFile(parser)),
    closeParseResult: () => dispatch(closeParseResult()),
    submitParseResult: () => dispatch(submitParseResult()),
    onSelectSignal: (index) => dispatch(selectSignal(index)),
    onSelectPlace: (signal, stage) => dispatch(selectPlace(signal, stage))
});

export const ExaminationEditor = connect(mapProps, mapActions)(ExaminationEditorControl);
