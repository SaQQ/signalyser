import { ExaminationsListState } from "./Selector/State";
import { EditingExaminationState } from "./Editor/State";

export interface ExaminationsState {
    readonly list: ExaminationsListState;
    readonly editing: EditingExaminationState;
}
