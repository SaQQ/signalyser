// THIS FILE IS GENERATED AUTOMATICALLY, DO NOT MODIFY


/**
 * API Result
 */
export interface CreatedResult {
    id: string;
}

export interface UpdatedResult {
    id: string;
}

export interface RecomputedResult {
    amount: number;
}

export type CreatedOrUpdated = CreatedResult | UpdatedResult;

export interface SignalDescriptor {
    name: string;
}

export interface StageDescriptor {
    name: string;
}

export interface TagDescriptor {
    name: string;
}

export interface ParameterDescriptor {
    name: string;
    formula: string;
    compiledFormula: string;
}

export interface Protocol {
    id: string;
    name: string;
    signals: SignalDescriptor[];
    stages: StageDescriptor[];
    tags: TagDescriptor[];
    params: ParameterDescriptor[];
}

export interface UpdateNamed {
    newName: string;
}

export interface ProtocolUpdate {
    id: string;
    name: string;
    signals: (SignalDescriptor & UpdateNamed)[];
    stages: (StageDescriptor & UpdateNamed)[];
    tags: (TagDescriptor & UpdateNamed)[];
    params: (ParameterDescriptor & UpdateNamed)[];
}

export interface ProtocolDescription {
    id: string;
    name: string;
}

export * from "./ExaminationContract";
