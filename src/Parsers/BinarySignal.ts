import { SignalData } from "../Contract";

// The format:
//  4B - Uint32 - N = Samples dimension
//  4B - Uint32 - C = Samples count per dimension
//  4B * C - Float32 - time data
//  4B * C * N - Float32 - samples (consecutive buffers per dimension)

interface BinaryBuffer {
    buffer: ArrayBuffer;
    offset: number;
}

function writeUint32(output: BinaryBuffer, value: number) {
    let view = new Uint32Array(output.buffer, output.offset, 1);
    view[0] = value;
    output.offset += Uint32Array.BYTES_PER_ELEMENT;
}

function readUint32(input: BinaryBuffer) {
    let view = new Uint32Array(input.buffer, input.offset, 1);
    input.offset += Uint32Array.BYTES_PER_ELEMENT;
    return view[0];
}

function writeArray(output: BinaryBuffer, value: ArrayLike<number>) {
    let view = new Float32Array(output.buffer, output.offset, value.length);
    view.set(value);
    output.offset += value.length * Float32Array.BYTES_PER_ELEMENT;
}

function readArray(input: BinaryBuffer, length: number) {
    let view = new Float32Array(input.buffer, input.offset, length);
    input.offset += length * Float32Array.BYTES_PER_ELEMENT;
    return view;
}

function calculateSize(signal: SignalData) {
    return Uint32Array.BYTES_PER_ELEMENT * 2 +
        (signal.seriesData.length + 1) * signal.timeData.length * Float32Array.BYTES_PER_ELEMENT;
}

function ensureLength(input: BinaryBuffer, length: number) {
    if (input.buffer.byteLength < length) {
        throw new Error("The signal data is invalid - the buffer is too small");
    }
}

export function save(signal: SignalData): ArrayBuffer {
    let output = {
        offset: 0,
        buffer: new ArrayBuffer(calculateSize(signal))
    };

    writeUint32(output, signal.seriesData.length);
    writeUint32(output, signal.timeData.length);
    writeArray(output, signal.timeData);

    for (let i = 0; i < signal.seriesData.length; i++) {
        writeArray(output, signal.seriesData[i]);
    }

    return output.buffer;
}

export function read(data: ArrayBuffer): SignalData {
    let input = { offset: 0, buffer: data };
    ensureLength(input, 8);

    let series = readUint32(input);
    let samples = readUint32(input);
    ensureLength(input, (series + 1) * samples * 4 + 8);

    let timeData = readArray(input, samples);
    let seriesData: ArrayLike<number>[] = new Array(series);

    for (let i = 0; i < series; i++) {
        seriesData[i] = readArray(input, samples);
    }

    return { timeData, seriesData };
}
