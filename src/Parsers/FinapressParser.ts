import { ParserReturnType, SignalAssignment, AttributesCollection, TagValue } from "./ParserSignature";

import * as fs from "fs";
import * as readline from "readline";
import * as csvParse from "csv-parse";

const SanitizeRegex = /(\(.+\)|[^a-zA-Z0-9])/i;

function sanitizeTag(name: string) {
    return name.split(" ")
        .map(t => t.replace(SanitizeRegex, ""))
        .filter(t => t !== "")
        .map(t => t[0].toUpperCase() + t.substr(1))
        .join("");
}

function tryParse(value: string) {
    let val = Number(value);
    return isNaN(val) ? value : val;
}

function stripQuote(value: string) {
    if (value.startsWith("\"")) {
        return value.substr(1, value.length - 2);
    } else {
        return value;
    }
}

class FinapressParser {
    private partLine = 0;
    private currentLine = 0;
    private state: 0 | 1 | 2 = 0;

    private tags: AttributesCollection<TagValue> = {};

    public line(line: string) {
        if (line === "") {
            this.partLine = 0;
            this.state++;
        } else {
            switch (this.state) {
                case 0:
                    this.parseDeviceLine(line);
                    break;
                case 1:
                    this.parsePatientLine(line);
                    break;
                case 2:
                    this.parseDataLine(line);
                    break;
            }
            this.partLine++;
        }
        this.currentLine++;
    }

    public completeResult(): ParserReturnType {
        if (this.validSignalsCount === 0) {
            throw new Error("There are no valid signals in the file");
        }

        const dataBuffer = this.buildDataArray();
        let timeBuffer: Float32Array | undefined;
        let signals: { name: string, data: Float32Array }[] = [];

        for (const sig of this.mapToSignal(dataBuffer)) {
            if (sig.name === "Time") {
                timeBuffer = sig.data;
            } else {
                signals.push(sig);
            }
        }

        if (timeBuffer === undefined) {
            timeBuffer = (<any>signals.shift()).data;
        }

        const assignments = signals.map(s => ({
            signal: "",
            stage: "",
            description: s.name,
            data: {
                timeData: <Float32Array>timeBuffer,
                seriesData: [s.data]
            }
        }));

        return [this.tags, assignments];
    }

    private parseDeviceLine(line: string) {
        const parts = line.split(":");
        if (parts.length !== 2) {
            throw new Error("Invalid device information value, each tag must be in `NAME: VALUE` format");
        }

        this.tags[sanitizeTag(parts[0])] = tryParse(parts[1]);
    }

    private patientTagNames: string[];
    private parsePatientLine(line: string) {
        if (this.partLine === 0) {
            this.patientTagNames = line.split(";").map(sanitizeTag);
        } else {
            const values = line.split(";")
                .map(v => tryParse(stripQuote(v)));

            if (values.length !== this.patientTagNames.length) {
                throw new Error("Invalid patient information - header has different fields than the values");
            }

            for (let i = 0; i < values.length; i++) {
                const h = this.patientTagNames[i];
                const v = values[i];
                if (v !== "") {
                    this.tags[h] = v;
                }
            }
        }
    }

    private dataNames: string[];
    private dataValues: number[][] = [];
    private samplesInColumn: number[];
    private parseDataLine(line: string) {
        // There is an additional semicolon at the end of each line, get rid of
        // it right away

        if (this.partLine === 0) {
            this.dataNames = line.split(";");
            this.dataNames.pop();

            this.samplesInColumn = new Array<number>(this.dataNames.length);
            for (let i = 0; i < this.dataNames.length; i++) {
                this.samplesInColumn[i] = 0;
            }
        } else {

            let parts = line.split(";");
            parts.pop();

            if (parts.length !== this.dataNames.length) {
                throw new Error(`Invalid data - line ${this.currentLine} has invalid number of values`);
            }

            const parsed = new Array<number>(parts.length);
            for (let i = 0; i < parts.length; i++) {
                parsed[i] = parseFloat(parts[i]);
                if (!isNaN(parsed[i])) {
                    this.samplesInColumn[i]++;
                }
            }
            this.dataValues.push(parsed);
        }
    }

    private buildDataArray() {
        const samplesPerSignal = this.dataValues.length;
        const validSignals = this.validSignalsCount;

        let outputBuffer = new Float32Array(validSignals * samplesPerSignal);

        for (let row = 0; row < samplesPerSignal; row++) {
            for (let column of this.validSignalsIdxs()) {
                const idx = column * samplesPerSignal + row;
                outputBuffer[idx] = this.dataValues[row][column];
            }
        }

        return outputBuffer;
    }

    private *mapToSignal(buffer: Float32Array) {
        const samplesPerSignal = this.dataValues.length;
        for (let column of this.validSignalsIdxs()) {
            const idx = column * samplesPerSignal;
            const subbuffer = buffer.subarray(idx, idx + samplesPerSignal);
            yield { name: this.dataNames[column], data: subbuffer };
        }
    }

    private *validSignalsIdxs() {
        const samplesPerSignal = this.dataValues.length;
        for (let i = 0; i < this.dataNames.length; i++) {
            if (this.samplesInColumn[i] === samplesPerSignal) {
                yield i;
            }
        }
    }

    private get validSignalsCount() {
        const samplesPerSignal = this.dataValues.length;
        return this.samplesInColumn.reduce((p, c) => p + (c === samplesPerSignal ? 1 : 0), 0);
    }
}

export default (filename: string, resolve: (result: ParserReturnType) => void, reject: (reason?: any) => void) => {


    const stream = fs.createReadStream(filename);
    const lineStream = readline.createInterface({ input: stream, terminal: false });

    const parser = new FinapressParser();
    lineStream.on("error", reject)
        .on("line", (line: string) => {
            try {
                parser.line(line);
            } catch (e) {
                reject(e);
            }
        })
        .on("close", () => {
            try {
                resolve(parser.completeResult());
            } catch (e) {
                reject(e);
            }
        });
};
