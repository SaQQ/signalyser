import { ParserSignature, ParserReturnType } from "./ParserSignature";

export default (filename: string, parser: ParserSignature): Promise<ParserReturnType> => {

    return new Promise<ParserReturnType>((resolve, reject) => {
        // Make sure the parser wont crash the app by throwing
        try {
            parser(filename, resolve, reject);
        } catch (e) {
            reject(e);
        }
    });

};
