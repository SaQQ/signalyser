import { ParserReturnType, SignalAssignment, AttributesCollection, TagValue } from "./ParserSignature";

import * as fs from "fs";
import * as readline from "readline";
import * as path from "path";

interface WinCPPRSFileCollectionInfo {
    readonly signalFiles: string[];
    readonly metaFile?: string;
}

function GetFileCollectionInfo(filename: string): WinCPPRSFileCollectionInfo {

    let { dir, name, ext } = path.parse(filename);

    let nameParts = name.split(" ");

    if (nameParts.length < 1) {
        throw "Invalid file name";
    }

    let examinationId = nameParts[0];
    let metaFile: string = "";

    if (examinationId.endsWith("_metaplik")) {
        examinationId = examinationId.substring(0, examinationId.length - "_metaplik".length - 1);
    }

    let signalFiles = fs.readdirSync(dir).filter(f => {
        let { name, ext } = path.parse(f);

        if (ext === ".dat" && name.startsWith(examinationId + " ")) {
            return true;
        } else if (ext === ".txt" && name === examinationId + "_metaplik") {
            metaFile = dir + "/" + f;
            return false;
        } else {
            return false;
        }
    }).map(f => dir + "/" + f);

    if (metaFile !== "") {
        return { signalFiles, metaFile };
    } else {
        return { signalFiles };
    }
}

const NameRegex = /^[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]+$/;
const DateRegex = /^[[\d]{4}[-:][\d]{2}[-:][\d]{2}$/;
const SexRegex = /^M|K$/;
const NumberRegex = /^\d*$/;

function interpretTags(tagValues: string[]): AttributesCollection<TagValue> {
    let tags: AttributesCollection<TagValue> = {};

    tagValues.forEach(t => {
        if (SexRegex.test(t)) {
            if ("Sex" in tags) {

            } else {
                tags["Sex"] = t;
            }
        } else if (NameRegex.test(t)) {
            if ("Name" in tags) {
                if ("Surname" in tags) {

                } else {
                    tags["Surname"] = t;
                }
            } else {
                tags["Name"] = t;
            }
        } else if (DateRegex.test(t)) {
            if ("BirthDate" in tags) {
                if ("Date" in tags) {

                } else {
                    tags["Date"] = t;
                }
            } else {
                tags["BirthDate"] = t;
            }
        } else if (NumberRegex.test(t)) {
            if ("Age" in tags) {

            } else {
                tags["Age"] = parseInt(t);
            }
        }
    });

    return tags;
}

export default (filename: string, resolve: (result: ParserReturnType) => void, reject: (reason?: any) => void) => {

    try {
        let fileInfo = GetFileCollectionInfo(filename);

        const readTasks = fileInfo.signalFiles.map(f => new Promise<[string, [number[], number[]]]>((resolve, reject) => {
            const stream = fs.createReadStream(f);
            const lineStream = readline.createInterface({ input: stream, terminal: false });

            let times: number[] = [];
            let samples: number[] = [];

            lineStream.on("error", () => { reject(new Error("Error while reading file " + f)); })
                .on("line", (line: string) => {
                    try {
                        let values = line.split(" ").filter(v => v !== "");
                        if (values.length !== 2) {
                            reject(new Error("Invalid format of file " + f));
                        }
                        try {
                            times.push(parseFloat(values[0]));
                            samples.push(parseFloat(values[1]));
                        } catch (e) {
                            reject(e);
                        }
                    } catch (e) {
                        reject(e);
                    }
                })
                .on("close", () => {
                    let { name } = path.parse(f);

                    resolve([name.split(" ").slice(1).join(" "), [times, samples]]);
                });
        }));

        Promise.all(readTasks).then(signals => {

            let tags: AttributesCollection<TagValue> = {};

            if (fileInfo.metaFile) {
                tags = interpretTags(fs.readFileSync(fileInfo.metaFile, "utf-8").split("/"));
            }

            resolve([tags, signals.map<SignalAssignment>(s => ({
                signal: "",
                stage: "",
                description: s[0],
                data: {
                    timeData: s[1][0],
                    seriesData: [s[1][1]]
                }
            }))]);
        });

    } catch (e) {
        reject(e);
    }

};
