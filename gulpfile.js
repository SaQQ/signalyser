'use strict';

let gulp = require('gulp');

let app = require('./gulp-app');
let server = require('./gulp-server');
let tests = require('./gulp-test');

gulp.task('clean', ['clean:app', 'clean:server', 'clean:test']);
gulp.task('clean:all', ['clean:app:all', 'clean:server:all']);
gulp.task('watch', ['watch:app']);
gulp.task('build', ['build:app', 'build:server', 'build:test']);
gulp.task('default', ['build']);

gulp.task('start', ['build:app', 'build:server'], () => {
    let processes = [];
    const killAll = () => processes.forEach(p => p.kill());

    processes = [app.spawnApp(killAll), server.spawnApp(killAll)];
});

gulp.task('watch', ['build:app', 'build:server'], () => {
    let killFuncs = [];
    const killAll = () => {
        killFuncs.forEach(f => f());
        process.exit();
    };

    killFuncs = [app.watchApp(killAll), server.watchApp(killAll)];
});
