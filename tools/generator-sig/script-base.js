"use strict";

let generators = require("yeoman-generator");
let findup = require('findup-sync');
let path = require("path");
let _ = require("lodash");
let ejs = require("ejs");

class SimpleActionGen extends generators.Base {
    constructor(args, options, actionTpl = "Action.ts", reducerTpl = "Reducer.ts") {
        super(args, options);

        this.actionTpl = actionTpl;
        this.reducerTpl = reducerTpl;

        this.argument("name", { type: String, required: true, desc: "The name of the action" });

        this.option("action", { type: String, desc: "Output action file" });
        this.option("reducer", { type: String, desc: "Output reducer file" });
        this.option("reexport", { type: Boolean, defaults: false, desc: "Re-export action in index.ts" })
    }

    _removeExtension(str) {
        return str.replace(/\.[^/.]+$/, "");
    }

    _locateRoot() {
        return path.dirname(findup('package.json'));
    }

    _relativeRoot() {
        return path.relative(this.destinationRoot(), this._locateRoot());
    }

    _getUtils() {
        return path.join(this._relativeRoot(), "Utils");
    }

    _getActionFile() {
        return this.destinationPath((this.options.action || this.name + ".ts"));
    }

    _getImportFile() {
        let p = this._removeExtension(path.relative(this.destinationRoot(), this._getActionFile()));

        if (p.startsWith(".")) {
            return p;
        } else {
            return "./" + p;
        }
    }

    _getReducerFile() {
        return this.destinationPath((this.options.reducer || this.name + "Reducers.ts"));
    }

    _getTplArgs() {
        return { utils: this._getUtils(), name: this.name, _ };
    }

    _shouldAppendAction() {
        return this.options.action !== undefined && this.fs.exists(this._getActionFile());
    }

    _shouldAppendReducer() {
        return this.options.reducer !== undefined && this.fs.exists(this._getReducerFile());
    }

    _append(file, content, sep = "\n\n") {
        let oldContent = this.fs.read(file).trim();
        let newContent = oldContent + sep + content + "\n";
        this.fs.write(file, newContent);
    }

    _appendTpl(tplPath, outFile, data) {
        let template = this.fs.read(this.templatePath(tplPath));
        let rendered = ejs.render(template, data).trim();

        this._append(outFile, rendered);
    }

    _writeOrAppend(outFile, content) {
        if (this.fs.exists(outFile)) {
            this._append(outFile, content, "\n");
        } else {
            this.fs.write(outFile, content);
        }
    }

    writing() {
        if (this._shouldAppendAction()) {
            this._appendTpl(
                this.templatePath("append-" + this.actionTpl),
                this._getActionFile(),
                this._getTplArgs()
            );
        } else {
            this.fs.copyTpl(
                this.templatePath(this.actionTpl),
                this._getActionFile(),
                this._getTplArgs()
            );
        }
        if (this._shouldAppendReducer()) {
            this._appendTpl(
                this.templatePath("append-" + this.reducerTpl),
                this._getReducerFile(),
                this._getTplArgs()
            );
        } else {
            this.fs.copyTpl(
                this.templatePath(this.reducerTpl),
                this._getReducerFile(),
                this._getTplArgs()
            );
        }

        if (this.options.reexport) {
            this._writeOrAppend(this.destinationPath("index.ts"), "export * from \"" + this._getImportFile() + "\";")
        }
    }
}
module.exports = { SimpleActionGen };
