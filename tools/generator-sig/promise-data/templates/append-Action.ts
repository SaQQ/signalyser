<% if (defineData) { %> export interface <%= data %> {
}<% } %>

interface <%= name %>Action extends PromiseDataAction<<%= result %>, <%= data %>> {
    type: "<%= name %>";
}
interface <%= name %>PendingAction extends PendingPromiseDataAction<<%= result %>, <%= data %>> {
    type: "<%= name %>_PENDING";
}
interface <%= name %>FulfilledAction extends FulfilledPromiseAction<<%= result %>> {
    type: "<%= name %>_FULFILLED";
}
interface <%= name %>RejectedAction extends RejectedPromiseAction<<%= result %>> {
    type: "<%= name %>_REJECTED";
}
export const is<%= name %> =
    (a: Action<<%= name %>Action>): a is <%= name %>Action => a.type === "<%= name %>";
export const is<%= name %>Pending =
    (a: Action<<%= name %>PendingAction>): a is <%= name %>PendingAction => a.type === "<%= name %>_PENDING";
export const is<%= name %>Fulfilled =
    (a: Action<<%= name %>FulfilledAction>): a is <%= name %>FulfilledAction => a.type === "<%= name %>_FULFILLED";
export const is<%= name %>Rejected =
    (a: Action<<%= name %>RejectedAction>): a is <%= name %>RejectedAction => a.type === "<%= name %>_REJECTED";

export type <%= name %>Result = <%= name %>PendingAction | <%= name %>FulfilledAction | <%= name %>RejectedAction;

export function <%= _.camelCase(name) %>(): <%= name %>Action {
    let promise: PromiseLike<<%= result %>> = undefined;
    let data: <%= data %> = undefined;
    return { type: "<%= name %>", payload: { promise, data } };
}
