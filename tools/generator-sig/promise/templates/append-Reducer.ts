import {
    <%= name %>Result,
    is<%= name %>Pending, is<%= name %>Fulfilled, is<%= name %>Rejected
} from "./<%= name %>";

export function <%= _.camelCase(name) %>Reducer(state: <%= reducerState %>, action: Action<<%= name %>Result>): <%= reducerState %> {
    if (is<%= name %>Pending(action)) {
        return state;
    } else if (is<%= name %>Fulfilled(action)) {
        return state;
    } else if (is<%= name %>Rejected(action)) {
        return state;
    }
    return state;
}
