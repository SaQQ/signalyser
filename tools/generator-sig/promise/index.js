"use strict";

let path = require("path");
let findup = require("findup-sync");

let base = require("../script-base");

module.exports = class extends base.SimpleActionGen {
    constructor(args, options) {
        super(args, options);

        this.option("result", { type: String, desc: "Result type", defaults: "{}" });
        this.option("reducerState", { type: String, desc: "The type of sub-state that the reducer operates on" });
    }

    _locateParentState() {
        return this._removeExtension(path.relative(this.destinationRoot(), findup("State.ts")));
    }

    _getTplArgs() {
        return Object.assign(super._getTplArgs(), {
            result: this.options.result,
            reducerState: this.options.reducerState || "any",
            reducerStatePath: this.options.reducerState !== undefined ? this._locateParentState() : undefined
        });
    }

    writing() {
        super.writing();
    }
}
