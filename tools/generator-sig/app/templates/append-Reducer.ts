import {
    <%= name %>Action, is<%= name %>
} from "./<%= name %>";

export const <%= _.camelCase(name) %>Reducer = createReducer({}, is<%= name %>, (_, a) => {});
