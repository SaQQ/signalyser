import { Action, ReduxAction, } from "<%= utils %>";

export interface <%= name %>Action extends ReduxAction {
    type: "<%= name %>";
}
export const is<%= name %> = (a: Action<<%= name %>Action>): a is <%= name %>Action => a.type === "<%= name %>";
export const <%= _.camelCase(name) %> = (): <%= name %>Action => ({ type: "<%= name %>" });
